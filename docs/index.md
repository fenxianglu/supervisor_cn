---
title: Supervisor中文网
hero:
  title: Supervisor
  description: Supervisor是一个进程管理工具，用于在类UNIX操作系统上控制和监视多个进程。能够自动启动、停止、重启和管理进程，并提供对进程日志的访问和监控。
  actions:
    - text: 起步
      link: /api/installing/
features:
  - title: '简单'
    emoji: 🚀
    description: Supervisor 通过一个简单易学的 `INI` 格式配置文件进行配置。提供了许多针对进程的选项，如重新启动失败的进程和自动日志回滚。
  - title: 集中化
    emoji: 💎
    description: Supervisor 提供一个统一的位置来启动、停止和监控进程。进程可以单独或以组的形式进行控制。可以配置 Supervisor 提供本地或远程的命令行和 Web 界面。
  - title: 兼容性
    emoji: 🌈
    description: Supervisor 几乎在所有除 Windows 以外的平台上都可以运行。经过在 Linux、Mac OS X、Solaris 和 FreeBSD 上的测试和支持。完全使用 Python 编写，因此安装不需要 C 编译器。
---
