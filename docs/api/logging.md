---
title: 日志
order: 8
---

supervisord执行的主要任务之一是日志记录。会记录活动日志，详细记录其运行过程中正在执行的操作。如果配置了相应选项，还会将子进程的标准输出（stdout）和标准错误输出（stderr）记录到其他文件中。

## 活动日志

活动日志是supervisord记录有关其自身健康状况、子进程状态变化、事件产生的任何消息以及调试和信息类消息的位置。活动日志的路径通过配置文件中 `[supervisord]` 部分的 `logfile` 参数进行配置，默认为 `$CWD/supervisord.log`。如果此选项的值是特殊字符串 `syslog`，则活动日志将被路由到 `syslog` 服务，而不是写入文件。以下示例显示了一些活动日志的样本内容。为了更好地适应屏幕，某些行已经换行处理。

### 活动日志输出示例

```log
2007-09-08 14:43:22,886 DEBG 127.0.0.1:Medusa (V1.11) started at Sat Sep  8 14:43:22 2007
        Hostname: kingfish
        Port:9001
2007-09-08 14:43:22,961 INFO RPC interface 'supervisor' initialized
2007-09-08 14:43:22,961 CRIT Running without any HTTP authentication checking
2007-09-08 14:43:22,962 INFO supervisord started with pid 27347
2007-09-08 14:43:23,965 INFO spawned: 'listener_00' with pid 27349
2007-09-08 14:43:23,970 INFO spawned: 'eventgen' with pid 27350
2007-09-08 14:43:23,990 INFO spawned: 'grower' with pid 27351
2007-09-08 14:43:24,059 DEBG 'listener_00' stderr output:
 /Users/chrism/projects/supervisor/supervisor2/dev-sandbox/bin/python:
 can't open file '/Users/chrism/projects/supervisor/supervisor2/src/supervisor/scripts/osx_eventgen_listener.py':
 [Errno 2] No such file or directory
2007-09-08 14:43:24,060 DEBG fd 7 closed, stopped monitoring <PEventListenerDispatcher at 19910168 for
 <Subprocess at 18892960 with name listener_00 in state STARTING> (stdout)>
2007-09-08 14:43:24,060 INFO exited: listener_00 (exit status 2; not expected)
2007-09-08 14:43:24,061 DEBG received SIGCHLD indicating a child quit
```

活动日志的“级别”通过配置文件中的 `loglevel` 参数在 `[supervisord]` ini文件部分进行配置。当设置了 `loglevel` 时，指定优先级及更高优先级的消息将被记录到活动日志中。例如，如果 `loglevel` 设置为 `error`，则只会记录错误和关键级别的消息。然而，如果`loglevel` 设置为 `warn`，则会记录警告、错误和关键级别的消息。

### 活动日志级别

下表详细描述了日志级别，按照从最高优先级到最低优先级的顺序排列。"Config File Value"是提供给配置文件中 `[supervisord]` 部分的`loglevel` 参数的字符串，而"Output Code"则是在活动日志输出行中显示的代码。

| 配置文件值 | 输出代码 | 描述 |
|:- |:- |:- |
| critical | CRIT | 指示需要立即引起用户注意、supervisor状态变化或supervisor本身出现错误的消息。 |
| error | ERRO | 指示可能可忽略的错误条件（例如，无法清除日志目录）的消息。 |
| warn | WARN | 指示异常条件，但不属于错误的消息。 |
| info | INFO | 正常的信息输出。如果没有明确配置日志级别，则默认为此级别。 |
| debug | DEBG | 对于试图调试进程配置和通信行为（进程输出、监听器状态变化、事件通知）的用户有用的消息。 |
| trace | TRAC | 对于试图调试supervisor插件以及有关HTTP和RPC请求和响应的开发人员有用的消息。 |
| blather | BLAT | 对于试图调试supervisor本身的开发人员有用的消息。 |

### 活动日志回滚

活动日志根据配置文件中 `[supervisord]` 部分的 `logfile_maxbytes` 和 `logfile_backups` 参数的组合，由 `supervisord` 进行“回滚”（即日志切割）。当活动日志达到 `logfile_maxbytes` 字节时，当前日志文件将被移到备份文件中，同时创建一个新的活动日志文件。在此过程中，如果现有的备份文件数大于或等于 `logfile_backups` 的值，则最旧的备份文件将被删除，并相应地重命名备份文件。如果正在写入的文件名为 `supervisord.log`，在其超过 `logfile_maxbytes` 后，该文件将被关闭并重命名为 `supervisord.log.1`，如果存在supervisord.log.1、supervisord.log.2等文件，则将依次重命名为supervisord.log.2、supervisord.log.3等。如果`logfile_maxbytes` 为 `0`，则日志文件永远不会进行回滚（因此也不会进行备份）。如果 `logfile_backups` 为 `0`，则不会保留备份文件。

## 子进程日志

默认情况下，supervisor会捕获由其生成的子进程的 `stdout`，并重新显示给 `supervisorctl` 和其他客户端的用户。如果在配置文件的 `[program:x]`、`[fcgi-program:x]` 或 `[eventlistener:x]` 部分没有进行特定的与日志文件相关的配置，则以下规则适用：

- supervisord将会把子进程的stdout和stderr输出捕获到临时文件中，每个流都会被捕获到一个单独的文件中。这被称为自动日志模式（AUTO log mode）。
- `AUTO` 日志文件会自动命名，并放置在配置文件的 `[supervisord]` 部分中 `childlogdir` 指定的目录中。
- 每个AUTO日志文件的大小受到程序部分的 `{streamname}_logfile_maxbytes` 值的限制（其中{streamname}可以是“stdout”或“stderr”）。当日志文件达到该大小时，根据 `{streamname}_logfile_backups` 进行轮转（类似于活动日志）。

影响在 `[program:x]` 和 `[fcgi-program:x]` 部分中子进程日志记录的配置键有以下几个：

`redirect_stderr`,` stdout_logfile`, `stdout_logfile_maxbytes`, `stdout_logfile_backups`, `stdout_capture_maxbytes`, `stdout_syslog`, `stderr_logfile`, `stderr_logfile_maxbytes`, `stderr_logfile_backups`, `stderr_capture_maxbytes`, 和 `stderr_syslog`。

`[eventlistener:x]` 部分可能不会指定 `redirect_stderr`、`stdout_capture_maxbytes` 或 `stderr_capture_maxbytes`，但除此之外，同样接受相同的值。

在 `[supervisord]` 配置文件部分中影响子进程日志记录的配置键有以下两个：`childlogdir` 和 `nocleanup`。

### 捕获模式

捕获模式是Supervisor的高级功能。除非基于解析自子进程输出的数据采取操作，否则无需理解捕获模式。

如果配置文件中的 `[program:x]` 部分定义了非零的 `stdout_capture_maxbytes` 或 `stderr_capture_maxbytes` 参数，那么由该程序部分表示的每个进程可以在其 `stdout` 或 `stderr` 流（分别）上发出特殊标记，将会导致supervisor实际上发出一个`PROCESS_COMMUNICATION` 事件（有关事件的描述，请参阅[Events](http://supervisord.org/events.html#events)）。

进程通信协议依赖于两个标记，一个用于命令supervisor进入流的“捕获模式”，另一个用于命令退出。当一个进程流进入“捕获模式”时，发送到该流的数据将被发送到内存中的一个单独缓冲区，即“捕获缓冲区”，该缓冲区最大允许包含 `capture_maxbytes` 字节。在捕获模式下，当缓冲区的长度超过 `capture_maxbytes` 字节时，最早的数据将被丢弃，以为新数据腾出空间。当进程流退出捕获模式时，supervisor会发出一个`PROCESS_COMMUNICATION` 事件子类型，可以被事件监听器拦截。

开始进入进程流的“捕获模式”的标记是 `<!--XSUPERVISOR:BEGIN-->`。退出捕获模式的标记是 `<!--XSUPERVISOR:END-->`。这些标记之间的数据可以是任意的，并形成 `PROCESS_COMMUNICATION` 事件的有效负载。例如，如果一个程序设置了 `stdout_capture_maxbytes` 为"1MB"，并且在其stdout流上发出以下内容：

```xml
<!--XSUPERVISOR:BEGIN-->Hello!<!--XSUPERVISOR:END-->
```

在这种情况下，supervisord将发出一个带有数据有效负载为"Hello!"的 `PROCESS_COMMUNICATIONS_STDOUT` 事件。

一个示例脚本（使用Python编写），会发出进程通信事件，可以在supervisor软件包的 `scripts` 目录中找到，名为 `sample_commevent.py`。

作为“事件监听器”（ `[eventlistener:x]` 部分）指定的进程的输出不会以这种方式进行处理。这些进程的输出不能进入捕获模式。