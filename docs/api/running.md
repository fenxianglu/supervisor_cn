---
title: 运行
order: 3
---

# 运行 Supervisor

在解释如何运行 `supervisord` 和 `supervisorctl` 命令时，此部分提到了一个 `BINDIR`。这是 Python 安装配置的“bindir”目录。例如，如果通过 `./configure --prefix=/usr/local/py; make; make install` 进行 Python 安装，则 `BINDIR` 将为 `/usr/local/py/bin`。不同平台上的 Python 解释器使用不同的 BINDIR。如果找不到 BINDIR，请查看 `setup.py install` 的输出。

## 添加一个程序

在 `supervisord` 起到实际作用之前，至少需要添加一个程序部分到其配置中。程序部分将定义在调用 `supervisord` 命令时要运行和管理的程序。要添加一个程序，需要编辑 `supervisord.conf` 文件。

其中一个最简单的可运行程序是 UNIX 的 `cat` 程序。下面显示了一个程序部分，该程序在 `supervisord` 进程启动时运行 `cat`。

```ini
[program:foo]
command=/bin/cat
```

这个部分可以被复制粘贴到 `supervisord.conf` 文件中。这是最简单的程序配置，因为只指定了一个命令。程序配置部分还有许多其他的配置选项，这里没有展示出来。请参阅[[program:x] 部分设置](http://supervisord.org/configuration.html#programx-section)以获取更多信息。


启动需加载配置文件：

```shell
supervisord -c /etc/supervisor/supervisord.conf
```

关闭命令：

```shell
supervisorctl shutdown
```

重新加载配置文件：

```shell
supervisorctl reload
```

其他：

```shell
supervisorctl status redis    # redis状态
supervisorctl stop redis      # 停止redis
supervisorctl start redis     # 启动redis
supervisorctl restart reids   # 重启redis
supervisorctl reload redis   # 重载redis


supervisorctl status all      # 查看所有进程状态
supervisorctl stop   all      # 停止所有进程
supervisorctl start  all      # 启动所有进程
supervisorctl restart all     # 重启所有进程
supervisorctl reload all     # 重载所有进程
```

直接进入命令面板操作：

```shell
supervisorctl
```

![](./images/supervisorctl.png)

后面的命令都可以直接在命令面板中输入使用

```shell
status redis    # redis状态
stop redis      # 停止redis
start redis     # 启动redis
restart reids   # 重启redis
reload redis   # 重载redis
```

### 运行 Node.js 应用

```shell
supervisor app.js
```

### 运行 Express 应用

```shell
supervisor ./bin/www
```

或者

结合 package.json

```json
{
  "scripts": {
    "start": "supervisor ./bin/www"
  }
}
```

然后

```shell
npm start
```

对应的配置文件示例：

```ini
[program:demo]
command=npm start
directory=/www/wwwroot/demo
autorestart=true
startsecs=3
startretries=3
stdout_logfile=/www/supervisor/log/demo.out.log
stderr_logfile=/www/supervisor/log/demo.err.log
stdout_logfile_maxbytes=2MB
stderr_logfile_maxbytes=2MB
user=root
priority=999
numprocs=1
process_name=%(program_name)s_%(process_num)02d
```

:::info{title=提示}
`02d` 表示该值将被格式化为一个两位的整数，如果不足两位，前面会用 `0` 填充。例如，如果 `process_num` 的值为 `5`，则这部分将被格式化为 `05`。
:::

### 运行 java 应用

创建一个名为 myapp.conf 的配置文件，包含以下内容：

```ini
[program:myapp]
command=/usr/bin/java -jar /path/to/your/app.jar
directory=/path/to/your/app/directory
user=your_user
autostart=true
autorestart=true
redirect_stderr=true
stdout_logfile=/path/to/your/app.log
```

- `myapp` 是你的应用程序的名称，可以根据实际情况进行修改。
- `command` 是运行 Java 应用程序的命令，需要替换为你自己的 Java 应用程序的启动命令。在这个例子中，假设你的应用程序是一个可执行的 JAR 文件。
- `directory` 是应用程序所在的目录路径。
- `user` 是应用程序运行时使用的用户，一般是 `root`。
- `autostart` 设置为 `true` 表示在 Supervisor 启动时自动启动该应用程序。
- `autorestart` 设置为 `true` 表示如果应用程序意外退出，Supervisor 会自动重新启动它。
- `redirect_stderr` 设置为 `true` 表示将错误日志重定向到标准输出。
- `stdout_logfile` 是指向应用程序的标准输出日志文件路径。

将 myapp.conf 配置文件放置在 Supervisor 的配置目录中（通常为 `/etc/supervisor/conf.d/` 目录）。

执行以下命令以重新加载 Supervisor 配置：

```shell
supervisorctl reread
supervisorctl update
```

或者

```shell
supervisorctl reload
```

更新 Supervisor 的配置，并启动的 Java 应用程序。

如果只是更新了 jar 包直接重启应用即可：

```
supervisorctl restart <应用名称>
```

## 运行 supervisord

要启动 supervisord，请运行 `$BINDIR/supervisord` 命令。生成的进程将使自己变为守护进程并与终端分离。默认情况下，会在
BINDIR/supervisord 命令。生成的进程将使自己变为守护进程并与终端分离。默认情况下，会在 `CWD/supervisor.log` 中保持操作日志。

可以通过在命令行中传递 `-n` 标志来以前台模式启动 `supervisord` 可执行文件。这对于调试启动问题非常有用。

:::warning{title=注意}
当 `supervisord` 启动时，将在包括当前工作目录在内的默认位置中搜索其配置文件。如果关注安全性，可能希望在 `supervisord` 命令之后使用 `-c` 参数指定配置文件的绝对路径，以确保没有人从包含恶意 `supervisord.conf` 文件的目录中运行 supervisor。如果以 root 身份启动 supervisor 而没有使用-c 参数，则会发出警告提示。
:::

要更改由 `supervisord` 控制的程序集，请编辑 `supervisord.conf` 文件，并使用 `kill -HUP` 或其他方式重新启动 `supervisord` 进程。该文件中有几个示例程序定义。

`supervisord` 命令接受许多命令行选项。每个命令行选项都会覆盖配置文件中的任何等效值。

### supervisord 命令行选项

**-c FILE, --configuration=FILE**

supervisord 配置文件的路径。

**-n, --nodaemon**

以前台模式运行 supervisord。

**-s, --silent**

不将输出重定向到标准输出(stdout)。

**-h, --help**

显示 supervisord 命令的帮助信息。

**-u USER, --user=USER**

UNIX 用户名或数字用户 ID。如果以 root 用户身份启动 `supervisord`，在启动过程中尽快切换到此用户。

**-m OCTAL, --umask=OCTAL**

八进制数（例如 022），表示 `supervisord` 启动后应使用的 [umask](http://supervisord.org/glossary.html#term-umask) 值。

**-d PATH, --directory=PATH**

当 `supervisord` 作为守护进程运行时，在守护化之前切换到此目录。

**-l FILE, --logfile=FILE**

用作 `supervisord` 活动日志的文件路径。

**-y BYTES, --logfile_maxbytes=BYTES**

`supervisord` 活动日志文件在进行轮转之前的最大大小。该值是后缀乘法，例如，"1"表示一个字节，"1MB"表示 1 兆字节，"1GB"表示 1 千兆字节。

**-z NUM, --logfile_backups=NUM**

保留的 `supervisord` 活动日志备份副本数量。每个日志文件的大小将为 `logfile_maxbytes` 指定的大小。

**-e LEVEL, --loglevel=LEVEL**

`supervisor` 应写入活动日志的日志级别。有效级别包括 `trace`、`debug`、`info`、`warn`、`error` 和 `critical`。

**-j FILE, --pidfile=FILE**

`supervisord` 应将其进程 ID（pid）写入的文件名。

**-i STRING, --identifier=STRING**

由各种客户端用户界面公开的此 `supervisor` 实例的任意字符串标识符。

**-q PATH, --childlogdir=PATH**

`supervisor` 将写入其 `AUTO` 模式子进程日志的目录路径（该目录必须事先存在）。

**-k, --nocleanup**

阻止 `supervisord` 在启动时执行清理操作（删除旧的 `AUTO` 进程日志文件）。

**-a NUM, --minfds=NUM**

在成功启动 `supervisord` 进程之前，必须可用的最小文件描述符数量。

**-t, --strip_ansi**

从所有子日志进程中删除 `ANSI` 转义序列。

**-v, --version**

将 supervisord 的版本号打印到标准输出(stdout)，然后退出。

**--profile_options=LIST**

用逗号分隔的选项列表进行分析。导致 `supervisord` 在分析器下运行，并基于选项输出结果，选项是以下内容的逗号分隔列表：`cumulative`、`calls`、`callers`。例如：`cumulative`, `callers`。

**--minprocs=NUM**

在成功启动 `supervisord` 进程之前，必须可用的最小操作系统进程槽位数量。

## 运行 supervisorctl

要启动 `supervisorctl`，请运行 `$BINDIR/supervisorctl` 命令。将出现一个 `shell`，允许控制当前由 `supervisord` 管理的进程。在提示符下输入 `help` 以获取有关支持的命令的信息。

当从命令行传递参数调用时，`supervisorctl` 可执行文件可以使用“一次性”命令。例如：`supervisorctl stop all`。如果命令行上存在参数，将阻止交互式 `shell` 的调用。相反，该命令将被执行，并且 `supervisorctl` 将以 `0` 的代码退出表示成功或运行，非零表示错误。例如：`supervisorctl status all` 如果任何单个进程未运行，则返回非零值。

如果以交互模式调用 `supervisorctl`，并且与需要身份验证的 `supervisord` 进行交互，将被要求输入身份验证凭据。

### supervisorctl 命令行选项

**-c, --configuration**

配置文件路径（默认为 `/etc/supervisord.conf`）。

**-h, --help**

打印使用说明并退出。

**-i, --interactive**

在执行命令后启动交互式shell。

**-s, --serverurl URL**

supervisord服务器监听的URL（默认为 `http://localhost:9001`）。

**-u, --username**

用于与服务器进行身份验证的用户名。

**-p, --password**

用于与服务器进行身份验证的密码。

**-r, --history-file**

保留 `readline` 历史记录（如果可用）。

**action [参数]**

Actions是像“tail”或“stop”这样的命令。如果指定了-i选项或命令行中没有指定动作，则会启动一个解释交互式输入的“shell”。使用动作“help”来了解可用的动作。

### supervisorctl的操作命令

**help**

打印可用操作的列表。

**help <action>**

打印有关<action>的帮助信息。

**add <name> […]**

激活配置中进程/进程组的任何更新。

**remove <name> […]**

从活动配置中移除进程/进程组。

**update**

根据需要重新加载配置文件并进行add/remove操作，并将重新启动受影响的程序。

**update all**

根据需要重新加载配置文件并进行添加/删除操作，并将重新启动受影响的程序。

**update <gname> […]**

更新特定的进程组，并将重新启动受影响的程序。

**clear <name>**

清除一个进程的日志文件。

**clear <name> <name>**

清除多个进程的日志文件。

**clear all**

清除所有进程的日志文件。

**fg <process>**

以前台模式连接到一个进程。按Ctrl+C退出前台模式。

**pid**

获取supervisord的PID。

**pid <name>**

按名称获取单个子进程的PID。

**pid all**

逐行获取每个子进程的PID。

**reload**

重新启动远程supervisord。

**reread**

重新加载守护程序的配置文件，而不进行add/remove（无需重新启动）。

**restart <name>**

重新启动一个进程。注意：`restart` 不会重新读取配置文件。要进行配置文件的重新加载，请查看reread和update命令。

**restart <gname>:***

重新启动指定进程组中的所有进程。注意：restart不会重新读取配置文件。要进行配置文件的重新加载，请查看reread和update命令。

**restart <name> <name>**

重新启动多个进程或进程组。注意：`restart` 不会重新读取配置文件。要重新读取配置文件，请参见reread和update命令。

**restart all**

重新启动所有进程。注意：`restart` 不会重新读取配置文件。要重新读取配置文件，请参见reread和update命令。

**signal**

无关于信号的帮助信息

**start <name>**

启动一个进程。

**start <gname>:***

启动指定进程组中的所有进程。

**start <name> <name>**

启动多个进程或进程组。

**start all**

启动所有进程。

**status**

获取所有进程的状态信息。

**status <name>**

按名称获取单个进程的状态。

**status <name> <name>**

获取多个指定名称进程的状态。

**stop <name>**

停止一个进程。

**stop <gname>:***

停止指定进程组中的所有进程。

**stop <name> <name>**

停止多个进程或进程组。

**stop all**

停止所有进程。

**tail [-f] <name> [stdout|stderr] (default stdout)**

输出进程日志的最后部分。例如：`tail -f <name>`（连续显示指定名称进程的stdout），按 `Ctrl-C` 退出。`tail -100 <name>`（显示指定名称进程stdout的最后100个字节）。`tail <name> stderr`（显示指定名称进程stderr的最后1600个字节）。

## Signals

可以向 `supervisord` 程序发送信号，使其在运行时执行特定的操作。

可以将这些信号中的任何一个发送给单个 `supervisord` 进程ID。该进程ID可以在配置文件的 `[supervisord]` 部分的 `pidfile` 参数所表示的文件中找到（默认情况下为 `$CWD/supervisord.pid`）。

### Signal处理程序

`SIGTERM`

supervisord及其所有子进程将关闭。这可能需要几秒钟的时间。

`SIGINT`

supervisord及其所有子进程将关闭。这可能需要几秒钟的时间

`SIGQUIT`

supervisord及其所有子进程将关闭。这可能需要几秒钟的时间。

`SIGHUP`

supervisord将停止所有进程，从找到的第一个配置文件重新加载配置，并启动所有进程。

`SIGUSR2`

supervisord将关闭并重新打开主要活动日志和所有子日志文件。

## 运行时安全性

开发人员已经尽最大努力确保以root身份运行的 `supervisord` 进程不会导致意外的权限提升。但请注意风险自负。与DJ Bernstein的[daemontools](http://supervisord.org/glossary.html#term-daemontools)相比，Supervisor并谨慎，因为 `supervisord` 允许在其配置文件中使用任意路径规范来写入数据。允许任意路径选择可能会导致由符号链接攻击引起的漏洞。在配置中指定路径时要小心。确保`supervisord` 配置文件不能被非特权用户读取或写入，并且由supervisor软件包安装的所有文件都具有合理的文件权限保护设置。此外，请确保 `PYTHONPATH` 设置正确，并且所有Python标准库文件都具有足够的文件权限保护。

## 在启动时自动运行supervisord

启动配置：

```shell
vim /usr/lib/systemd/system/supervisord.service
[Unit]
Description=Process Monitoring and Control Daemon
After=rc-local.service nss-user-lookup.target

[Service]
Type=forking
ExecStart=/usr/bin/supervisord -c /etc/supervisord.conf

[Install]
WantedBy=multi-user.target
```

激活命令：

```shell
systemctl enable supervisord
systemctl is-enabled supervisord
```

如果使用的是发行版打包的Supervisor版本，应该已经集成到所使用发行版的服务管理基础设施中。

有适用于各种操作系统的用户贡献脚本，可以在以下位置找到：
[https://github.com/Supervisor/initscripts](https://github.com/Supervisor/initscripts)

如果遇到问题，可以在Serverfault上找到一些答案。
[在Linux（Ubuntu）上如何自动启动supervisord](http://serverfault.com/questions/96499/how-to-automatically-start-supervisord-on-linux-ubuntu)
