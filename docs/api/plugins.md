---
title: 第三方应用程序和库
order: 7
---

有许多第三方应用程序可以与 Supervisor 一起使用，这些应用程序可能会很有用。以下列表旨在总结并使其更易于查找。

请参阅 README.rst 以获取有关如何贡献至此列表的信息。

## 多个 Supervisor 实例的仪表板和工具

这些工具可以监控或控制运行在不同服务器上的多个 Supervisor 实例。

[cesi](https://github.com/Gamegos/cesi)

使用 Python 编写的基于 Web 的仪表板。

[Django-Dashvisor](https://github.com/aleszoulek/django-dashvisor)

使用 Python 编写的基于 Web 的仪表板。需要 Django 1.3 或 1.4。

[Nodervisor](https://github.com/TAKEALOT/nodervisor)

使用 Node.js 编写的基于 Web 的仪表板。

[Supervisord-Monitor](https://github.com/mlazarov/supervisord-monitor)

基于 PHP 编写的 Web 仪表板。

[SupervisorUI](https://github.com/luxbet/supervisorui)

另一个基于 PHP 编写的 Web 仪表板。

[supervisorclusterctl](https://github.com/RobWin/supervisorclusterctl)

使用 Ansible 控制多个 Supervisor 实例的命令行工具。

[suponoff](https://github.com/GambitResearch/suponoff)

基于 Python 3 编写的 Web 仪表板。需要 Django 1.7 或更高版本。

[Supvisors](https://github.com/julien6387/supvisors)

设计用于分布式应用程序，使用 Python 3.6 编写。包括扩展的 XML-RPC API、基于 Web 的仪表板和特殊功能，如分阶段启动和停止。

[multivisor](https://github.com/tiagocoutinho/multivisor)

集中式的 Supervisor Web 仪表板。前端基于 VueJS。后端运行一个 Flask Web 服务器。通过基于 zerorpc 的专用 Supervisor 事件监听器与每个 Supervisor 进程进行通信。

[Dart](https://github.com/plockaby/dart)

基于 Python 编写的使用 PostgreSQL 的 Web 仪表板和命令行工具，具备 REST API、事件监视和配置管理功能。

## Supervisor 的第三方插件和库。

这些是为 Supervisor 添加新功能的插件和库，其中还包括各种事件监听器。

[superlance](https://pypi.org/pypi/superlance/)

提供一组常见的事件监听器，可用于监视并在需要时重新启动，例如当进程使用过多内存时。

[superhooks](https://pypi.org/project/superhooks/)

将 Supervisor 事件通知发送到 HTTP1.1 Webhooks。

[mr.rubber](https://github.com/collective/mr.rubber)

一个事件监听器，使得在 Supervisor 主机上可以根据核心数自动调整进程数量。

[supervisor-wildcards](https://github.com/aleszoulek/supervisor-wildcards)

实现了对 Supervisor 的带有通配符支持的启动/停止/重启命令。这些命令可以并行运行，速度比内置的 start/stop/restart 命令更快。

[mr.laforge](https://github.com/fschulze/mr.laforge)

可以轻松确保 `supervisord` 以及由其控制的特定进程在 `shell` 和 `Python` 脚本中运行。还为 `supervisor` 添加了一个 `kill` 命令，可以向子进程发送任意信号。

[supervisor_cache](https://github.com/mnaberez/supervisor_cache)

这是一个 Supervisor 的扩展，提供了在 Supervisor 实例内部作为键值对直接缓存任意数据的功能。同时也可作为编写 Supervisor 扩展的参考。

[supervisor_twiddler](https://github.com/mnaberez/supervisor_twiddler)

这是 Supervisor 的一个 RPC 扩展，允许以通常在运行时不可实现的方式操纵 Supervisor 的配置和状态。

[supervisor-stdout](https://github.com/coderanger/supervisor-stdout)

一个事件监听器，将进程输出发送到 supervisord 的标准输出（stdout）。

[supervisor-serialrestart](https://github.com/native2k/supervisor-serialrestart)

为 `supervisorctl` 添加了一个 `serialrestart` 命令，该命令按顺序逐个重启进程，而不是一次性全部重启。

[supervisor-quick](http://lxyu.github.io/supervisor-quick/)

为 `supervisorctl` 添加了 `quickstart`、`quickstop` 和 `quickrestart` 命令，这些命令可以比内置命令更快。通过使用 XML-RPC 方法的非阻塞模式并轮询 `supervisord` 来实现。而内置命令使用阻塞模式，由于 `supervisord` 的实现细节，可能较慢。

[supervisor-logging](https://github.com/infoxchange/supervisor-logging)

一个事件监听器，将进程日志事件发送到外部的 Syslog 实例（例如 Logstash）。

[supervisor-logstash-notifier](https://github.com/dohop/supervisor-logstash-notifier)

一个事件监听器插件，用于将状态事件流式传输到 Logstash 实例。

[supervisor_cgroups](https://github.com/htch/supervisor_cgroups)

一个事件监听器，可以将 `Supervisor` 进程与 `cgroup` 层次结构关联起来。旨在作为 `cgrules.conf` 的替代品使用。

[supervisor_checks](https://github.com/vovanec/supervisor_checks)

这是一个用于构建基于 Supervisor 服务的健康检查的框架。健康检查应用程序应作为 Supervisor 环境中的事件监听器运行。在检查失败时，Supervisor 将尝试重新启动被监视的进程。

[Superfsmon](https://github.com/timakro/superfsmon)

监视一个目录，在文件发生变化时重新启动程序。可以监控目录的变化，通过通配符模式或正则表达式筛选文件路径，并单独或按组重新启动 Supervisor 程序。

## 与 Supervisor 集成第三方应用程序的库。

这些是使得与第三方应用程序一起使用 Supervisor 更加简便的库和插件：

[collective.recipe.supervisor](https://pypi.org/pypi/collective.recipe.supervisor/)

一个用于安装 Supervisor 的 buildout 配方。

[puppet-module-supervisor](https://github.com/plathrop/puppet-module-supervisor)

用于配置 Supervisor 守护进程工具的 Puppet 模块。

[puppet-supervisord](https://github.com/ajcrowe/puppet-supervisord)

用于管理 supervisord 进程控制系统的 Puppet 模块。

[ngx_supervisord](https://github.com/FRiCKLE/ngx_supervisord)

一个 Nginx 模块，提供与 supervisord 进行通信并按需管理（启动/停止）后端的 API。

[Supervisord-Nagios-Plugin](https://github.com/Level-Up/Supervisord-Nagios-Plugin)

一个用 Python 编写的 Nagios/Icinga 插件，用于监控单个 supervisord 进程。

[nagios-supervisord-processes](https://github.com/blablacar/nagios-supervisord-processes)

一个用 PHP 编写的 Nagios/Icinga 插件，用于监控单个 supervisord 进程。

[supervisord-nagios](https://github.com/3dna/supervisord-nagios)

一个用于 supervisorctl 的插件，允许进行类似 Nagios 的检查来监控 supervisord 管理的进程。

[php-supervisor-event](https://github.com/mtdowling/php-supervisor-event)

用于与 Supervisor 事件通知进行交互的 PHP 类。

[PHP5 Supervisor wrapper](https://github.com/yzalis/Supervisor)

用于将 Supervisor 实例作为对象管理的 PHP 5 库。

[Symfony2 SupervisorBundle](https://github.com/yzalis/SupervisorBundle)

将 Supervisor 多个服务器管理完全集成到 Symfony2 项目中。

[sd-supervisord](https://github.com/robcowie/sd-supervisord)

[Server Density](http://www.serverdensity.com/) 插件用于监控 supervisor。

[node-supervisord](https://github.com/crcn/node-supervisord)

Supervisor 的 XML-RPC 接口的 Node.js 客户端。

[node-supervisord-eventlistener](https://github.com/sugendran/node-supervisord-eventlistener)

Supervisor 的事件监听器的 Node.js 实现。

[ruby-supervisor](https://github.com/schmurfy/ruby-supervisor)

Supervisor 的 XML-RPC 接口的 Ruby 客户端库。

[Sulphite](https://github.com/jib/sulphite)

将 supervisord 事件发送到 [Graphite](https://github.com/graphite-project/graphite-web)。

[supervisord.tmbundle](https://github.com/countergram/supervisord.tmbundle)

[TextMate](http://macromates.com/) 的 supervisord.conf 包。

[capistrano-supervisord](https://github.com/yyuu/capistrano-supervisord)

[Capistrano](https://github.com/capistrano/capistrano) 配方用于部署基于 supervisord 的服务。

[capistrano-supervisor](https://github.com/glooby/capistrano-supervisor)

另一个用于通过 [Capistrano](https://github.com/capistrano/capistrano) 控制 supervisord 的包。

[chef-supervisor](https://github.com/opscode-cookbooks/supervisor)

[Chef](http://www.opscode.com/chef/) 的 cookbook，用于安装和配置 supervisord。

[SupervisorPHP](http://supervisorphp.com/)

完整的 PHP Supervisor 套件：包括使用 XML-RPC 接口的客户端、事件监听器和配置构建实现、控制台应用程序和监视器界面。

[Supervisord-Client](http://search.cpan.org/~skaufman/Supervisord-Client)

Supervisor 的 XML-RPC 接口的 Perl 客户端。

[supervisord4j](https://github.com/satifanie/supervisord4j)

Supervisor 的 XML-RPC 接口的 Java 客户端。

[Supermann](https://github.com/borntyping/supermann)

Supermann 监控在 Supervisor 下运行的进程，并将指标发送到 [Riemann](http://riemann.io/)。

[gulp-supervisor](https://github.com/leny/gulp-supervisor)

将 Supervisor 作为 Gulp 任务运行。

[Yeebase.Supervisor](https://github.com/yeebase/Yeebase.Supervisor)

从 TYPO3 Flow 应用程序中控制和监控 Supervisor。

[dokku-supervisord](https://github.com/statianzo/dokku-supervisord)

[Dokku](https://github.com/progrium/dokku) 插件，用于注入 `supervisord` 来运行应用程序。

[dokku-logging-supervisord](https://github.com/sehrope/dokku-logging-supervisord)

[Dokku](https://github.com/progrium/dokku) 插件，用于注入 `supervisord` 来运行应用程序。还将进程的 `stdout` 和 `stderr` 重定向到日志文件（而不是 Docker 的默认每个容器的 JSON 文件）。

[superslacker](https://github.com/MTSolutions/superslacker)

将 Supervisor 事件通知发送到 [Slack](https://slack.com/)。

[supervisor-alert](https://github.com/rahiel/supervisor-alert)

通过 [Telegram](https://telegram.org/) 或发送到任意命令的方式发送事件通知。

[supervisor-discord](https://github.com/chaos-a/supervisor-discord)

通过 Webhooks 将事件通知发送到 [Discord](https://discord.com/)。