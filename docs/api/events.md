---
title: 事件
order: 5
---

事件是Supervisor在3.0版本中引入的高级功能。如果只希望将Supervisor用作重新启动崩溃进程的机制或用作手动控制进程状态的系统，则不需要了解事件。但是，如果希望将Supervisor作为进程监控/通知框架的一部分使用，则需要了解事件。

## 事件监听器和事件通知

Supervisor提供了一种方式，让一个特殊编写的程序（作为其子进程运行）称为“事件监听器”，可以订阅“事件通知”。事件通知意味着与由supervisord控制的子进程或supervisord本身相关的事情发生了。为了使事件监听器能够订阅有限的事件通知子集，事件通知被分组为不同类型。即使没有配置任何监听器，Supervisor仍会在其运行时持续发出事件通知。如果配置了并订阅了属于supervisord生命周期中发出的某个事件类型的监听器，该监听器将收到通知。

事件通知/订阅系统的目的是在满足某些条件时提供运行任意代码（例如发送电子邮件、进行HTTP请求等）的机制。这个条件通常与子进程的状态有关。例如，当一个进程崩溃并由Supervisor重新启动时，可能希望通过电子邮件通知某人。

事件通知协议基于通过子进程的标准输入(stdin)和标准输出(stdout)进行通信。Supervisor向事件监听器进程的标准输入发送特定格式的输入，并期望从事件监听器的标准输出接收特定格式的输出，形成请求-响应循环。supervisor和监听器实现者之间达成的协议允许监听器处理事件通知。事件监听器可以用正在运行Supervisor的平台所支持的任何语言编写。虽然事件监听器可以使用任何语言编写，但针对Python有特殊的库支持，即 `supervisor.childutils` 模块，使得在Python中创建事件监听器比其他语言稍微容易一些。

### 配置事件监听器

在配置文件中，事件监听器通过 `[eventlistener:x]` 部分进行指定。Supervisor的 `[eventlistener:x]` 部分与 `[supervisor:program:x]` 部分几乎完全相同，在其配置中允许的键方面存在一些差异，唯一的例外是Supervisor不会对事件监听器进程的“捕获模式”输出做出响应（即，事件监听器不能作为 `PROCESS_COMMUNICATIONS_EVENT` 事件生成器）。因此，在事件监听器的配置中指定 `stdout_capture_maxbytes` 或 `stderr_capture_maxbytes` 将导致错误。对于可以放置在配置文件中的eventlistener部分数量，没有人为限制。

当定义了 `[eventlistener:x]` 部分时，实际上定义了一个“池”，其中事件监听器在池中的数量由该部分内的numprocs值确定。

`[eventlistener:x]` 部分的events参数指定将发送到监听器池的事件。一个良好编写的事件监听器会忽略无法处理的事件，但不能保证特定的事件监听器不会因为接收到无法处理的事件类型而崩溃。因此，根据监听器的实现方式，在配置中指定只能接收某些类型的事件可能很重要。事件监听器的实现者是唯一能告诉这些内容（因此也可以告诉在事件配置中放置什么值）的人。下面是可以放置在 `supervisord.conf` 中的eventlistener配置示例：

```ini
[eventlistener:memmon]
command=memmon -a 200MB -m bob@example.com
events=TICK_60
```

```ini
[eventlistener:mylistener]
command=my_custom_listener.py
events=PROCESS_STATE,TICK_60
```

:::info{title=提示}
一个高级特性是通过 `[eventlistener:x]` 部分的 `result_handler` 参数来指定池的替代“结果处理程序”，其形式为 `pkg_resources` 的“入口点”字符串。默认的结果处理程序是 `supervisord.dispatchers:default_handler`。目前没有文档说明如何创建替代的结果处理程序。
:::

当Supervisor发送事件通知时，将找到订阅接收该事件类型的所有事件监听器池（根据事件监听器部分中的 `events` 值进行过滤）。每个监听器池中的一个监听器将接收到事件通知（任何“可用”的监听器）。

Supervisor对事件监听器池中的每个进程都进行平等处理。如果池中的某个进程不可用（因为正在处理另一个事件、已经崩溃或选择从池中移除），Supervisor将会选择池中的另一个进程。如果无法发送事件，因为池中的所有监听器都“忙”，则事件将被缓冲，并在稍后重新尝试通知。这里的“稍后”是指“下一次supervisord选择循环执行”的时候。为了满意的事件处理性能，应该根据负载情况配置适量的事件监听器进程来处理事件。对于任何给定的工作负载，只能通过实证确定最佳的监听器数量，没有所谓的“魔法数字”。为了帮助确定给定池中最优监听器数量，当由于池拥塞导致无法立即发送事件时，Supervisor会向其活动日志发出警告消息。对于可以放入池中的进程数量，没有人为限制，仅受到平台限制的限制。

监听器池拥有一个事件缓冲队列。队列的大小通过监听器池的 `buffer_size` 配置文件选项进行设置。如果队列已满并且Supervisor尝试缓冲一个事件，Supervisor将丢弃缓冲区中最旧的事件并记录一个错误。

### 编写事件监听器

事件监听器的实现是一个愿意接受结构化输入并在其标准输入流上产生结构化输出的程序。事件监听器的实现应该以“无缓冲”模式进行操作，或者每次需要与supervisord进程进行通信时都应刷新其标准输出。事件监听器可以被编写成长期运行的，也可以在单个请求后退出（这取决于实现和事件监听器配置中的 `autorestart` 参数）。

事件监听器可以将任意输出发送到其标准错误(stderr)，该输出将根据其 `[eventlistener:x]` 部分中与标准错误相关的日志文件配置被supervisord记录或忽略。

#### 事件通知协议

当supervisord向事件监听器进程发送通知时，首先会在其标准输入(stdin)上发送一行“头部”信息。该行的组成是一组以冒号分隔的标记（每个标记表示一个键值对），标记之间用一个空格分隔。行以 `\n`（换行）字符结尾。行上的标记不保证按任何特定顺序排列。目前定义的标记类型如下表所示：

##### 头部标记

| 键 | 描述 | 示例 |
|:- |:- |:- |
| ver | 事件系统协议版本 | 3.0 |
| server | 发送事件的supervisord标识符（参见配置文件的[supervisord]部分identifier值）。 |  |
| serial | 为每个事件分配的整数值。在supervisord进程的生命周期内，没有两个事件具有相同的序列号。该值对于功能测试和检测事件顺序异常非常有用。 | 30 |
| pool | 生成此事件的事件监听器池的名称。 | myeventpool |
| poolserial | 由发送事件的事件监听器池为每个事件分配的整数值。在supervisord进程的生命周期内，由同一事件监听器池生成的两个事件不会具有相同的poolserial编号。该值可用于检测事件顺序异常。 | 30 |
| eventname | 具体的事件类型名称（参见 [事件类型](http://supervisord.org/events.html#event-types)）。 | TICK_5 |
| len | 一个整数，指示事件负载（即PAYLOAD_LENGTH）中的字节数。 | 22 |

以下是一个完整的头部行的示例：

```ini
ver:3.0 server:supervisor serial:21 pool:listener poolserial:10 eventname:PROCESS_COMMUNICATION_STDOUT len:54
```

在头部的换行符字符之后，紧接着是事件负载。由 `PAYLOAD_LENGTH` 字节组成，表示事件数据的序列化。有关特定事件数据序列化定义，请参见 [事件类型](http://supervisord.org/events.html#event-types)。

`PROCESS_COMMUNICATION_STDOUT` 事件通知的负载示例如下所示。

```ini
processname:foo groupname:bar pid:123
This is the data that was sent between the tags
```

任何给定事件的负载结构仅由该事件的类型确定。

#### 事件监听器的状态

事件监听器进程由supervisord维护着三种可能的状态：

| 名称 | 描述 |
|:- |:- |
| ACKNOWLEDGED | 事件监听器已确认（接受或拒绝）一个事件的发送。 |
| READY | 事件通知可能会发送到此事件监听器。 |
| BUSY | 事件通知可能不会发送到此事件监听器。 |

当事件监听器进程首次启动时，supervisord会自动将其置于 `ACKNOWLEDGED` 状态，以允许进行启动活动或防止启动失败（挂起）。直到监听器向其标准输出(stdout)发送 `READY\n` 字符串为止，将保持在此状态中。

当supervisord向处于 `READY` 状态的监听器发送事件通知时，监听器将被置于 `BUSY` 状态，直到接收到监听器的 `OK` 或 `FAIL` 响应。在此时，监听器将过渡回 `ACKNOWLEDGED` 状态。

#### 事件监听器通知协议

Supervisor会通过向处于 `READY` 状态的事件监听器进程的标准输入(stdin)发送数据来通知其发生的事件。在进程处于 `BUSY` 或 `ACKNOWLEDGED` 状态时，Supervisor不会向事件监听器进程的标准输入发送任何内容。Supervisor首先发送头部信息。

一旦事件监听器实现程序处理完头部信息，应该从标准输入(stdin)读取 `PAYLOAD_LENGTH` 字节，并根据头部中的值和解析出的序列化数据执行任意操作。在执行此过程中，可以自由地阻塞任意长的时间。Supervisor会在等待响应时继续正常处理，并在需要时向同一事件监听器池中的其他监听器进程发送相同类型的其他事件。

在事件监听器处理完事件序列化后，为了向supervisord通知结果，应该通过标准输出(stdout)发送一个结果结构。结果结构由单词"RESULT"、一个空格、结果长度、一个换行符和结果内容组成。例如，`RESULT 2\nOK` 表示结果为"OK"。按照惯例，事件监听器通常使用 `OK` 或 `FAIL` 作为结果内容。这些字符串对于默认的结果处理程序具有特殊意义。

如果默认的结果处理程序接收到 `OK` 作为结果内容，将假设事件监听器成功处理了事件通知。如果接收到 `FAIL` ，将假设监听器无法处理该事件，并将事件重新缓冲并在稍后重新发送。事件监听器可以基于任何原因返回 `FAIL` 来拒绝该事件。这不表示事件数据或事件监听器存在问题。一旦supervisord接收到 `OK` 或 `FAIL` 的结果，事件监听器将被置于 `ACKNOWLEDGED` 状态。

一旦监听器处于 `ACKNOWLEDGED` 状态，可以选择退出（随后如果其 `autorestart` 配置参数为 `true`，则可能由supervisor重新启动），或者可以继续运行。如果继续运行，并希望被supervisord放回 `READY` 状态，必须通过标准输出(stdout)发送一个 `READY` 标记，紧接着是一个换行符。

#### 事件监听器实现示例

以下是一个接受事件通知、将标题和载荷打印到其 stderr，并以 OK 结果和随后的 READY 响应的“长期运行”事件监听器的 Python 实现示例。

```py
import sys

def write_stdout(s):
    # only eventlistener protocol messages may be sent to stdout
    sys.stdout.write(s)
    sys.stdout.flush()

def write_stderr(s):
    sys.stderr.write(s)
    sys.stderr.flush()

def main():
    while 1:
        # transition from ACKNOWLEDGED to READY
        write_stdout('READY\n')

        # read header line and print it to stderr
        line = sys.stdin.readline()
        write_stderr(line)

        # read event payload and print it to stderr
        headers = dict([ x.split(':') for x in line.split() ])
        data = sys.stdin.read(int(headers['len']))
        write_stderr(data)

        # transition from READY to ACKNOWLEDGED
        write_stdout('RESULT 2\nOK')

if __name__ == '__main__':
    main()
```

在 [Superlance](http://supervisord.org/glossary.html#term-Superlance) 包中还有其他示例的事件监听器，其中包括一个可以监视 supervisord 的子进程，并在进程使用“过多”内存时重新启动该进程的监听器。

#### 事件监听器的错误条件

如果事件监听器进程在事件传输到其 `stdin` 时死亡，或者在向 `supervisord` 发送结果结构之前死亡，那么假定该事件未被处理，并且 `supervisord` 将对其进行重新缓冲，并稍后再次发送。

如果事件监听器向其 `stdout` 发送的数据不被 `supervisord` 认为是基于事件监听器的状态的适当响应，那么事件监听器将被置于 `UNKNOWN` 状态，并且不会再向其发送任何进一步的事件通知。如果在此期间监听器正在处理事件，则该事件将被重新缓冲并稍后再次发送。

#### 其他事项

事件监听器可以使用 `Supervisor` 的 XML-RPC 接口调用 "回调" 到 `Supervisor`。因此，事件监听器可以通过接收事件通知来影响 `Supervisor` 子进程的状态。例如，可能希望每隔几分钟生成与 `Supervisor` 控制的子进程的进程使用相关的事件，并且如果其中任何一个进程超过某个内存阈值，希望重新启动。可以编写一个程序，使 `supervisord` 定期生成包含内存信息的 `PROCESS_COMMUNICATION` 事件，并编写一个事件监听器根据处理从这些事件接收到的数据执行操作。

## 事件类型

事件类型是由 `Supervisor` 自身定义的一组受控类型。除非更改 `supervisord` 本身，否则无法添加事件类型。然而，通常情况下这并不是一个问题，因为事件附加了元数据，可以与其类型一起被事件监听器用作额外的过滤条件。

事件监听器可以订阅的事件类型由 `supervisord` 预定义，并分为几个主要类别，包括“进程状态变化”、“进程通信”和“supervisord状态变化”事件。下面是描述这些事件类型的表格。

在下面的列表中，我们指出一些事件类型具有一个名为 "body" 的令牌集。令牌集由一组以空格分隔的字符组成。每个令牌表示一个键值对。键和值之间用冒号分隔。例如：

```ini
processname:cat groupname:cat from_state:STOPPED
```

令牌集不包含换行符或回车符作为其结尾。

### `EVENT`事件类型

基本事件类型。这个事件类型是抽象的，永远不会直接发送。订阅此事件类型将导致订阅者接收到 Supervisord 发出的所有事件通知。

- 名称: `EVENT`
- 子类型: N/A
- 主体描述: N/A

### `PROCESS_STATE`事件类型

该进程类型表示一个进程从一个状态转移到另一个状态。请参阅进程状态以了解进程在其生命周期中经历的状态的描述。这个事件类型是抽象的，永远不会直接发送。订阅此事件类型将使订阅者接收到所有 `PROCESS_STATE` 子类型的事件通知。

- 名称: `PROCESS_STATE`
- 子类型: `EVENT`

#### 主体描述

所有 `PROCESS_STATE` 的子类型都有一个令牌集作为其主体。此外，每个 `PROCESS_STATE` 子类型的令牌集都有一组默认的键值对：`processname`、`groupname` 和 `from_state`。`processname` 表示 `supervisord` 所知道的该进程的进程名称。`groupname` 表示该进程所在的 `supervisord` 组的名称。`from_state` 是该进程从中转换的状态的名称（新状态由具体事件类型隐含）。具体的子类型可能还会在令牌集中包含其他的键值对。

### `PROCESS_STATE_STARTING`事件类型

表示进程已从某个状态转移到 `STARTING` 状态。

- 名称: `PROCESS_STATE_STARTING`
- 子类型: `PROCESS_STATE`

#### 主体描述

该主体是一个令牌集。包括默认的键值对以及额外的 `tries` `键。tries` 表示该进程在转换为 `RUNNING` 或 `FATAL` 前进入该状态的次数（永远不会超过进程的 "startretries" 参数）。例如：

```ini
processname:cat groupname:cat from_state:STOPPED tries:0
```

### `PROCESS_STATE_RUNNING`事件类型

表示进程已从 `STARTING` 状态转移到 `RUNNING` 状态。这意味着根据 `Supervisord` 的判断，该进程已成功启动。

- 名称: `PROCESS_STATE_RUNNING`
- 子类型: `PROCESS_STATE`

#### 主体描述

该主体是一个令牌集。包括默认的键值对以及额外的 pid 键。pid 表示已启动进程的 UNIX 进程 ID。例如：

```ini
processname:cat groupname:cat from_state:STARTING pid:2766
```

### `PROCESS_STATE_BACKOFF`事件类型

- 名称: `PROCESS_STATE_BACKOFF`
- 子类型: `PROCESS_STATE`

#### 主体描述

该主体是一个令牌集。包括默认的键值对以及额外的 `tries` 键。`tries` 表示进程在转换为 `RUNNING` 或 `FATAL` 前进入此状态的次数（永远不会大于进程的 "startretries" 参数）。例如：

```ini
processname:cat groupname:cat from_state:STOPPED tries:0
```

### `PROCESS_STATE_STOPPING`事件类型

表示进程已从 `RUNNING` 状态或 `STARTING` 状态转移到 `STOPPING` 状态。

- 名称: `PROCESS_STATE_STOPPING`
- 子类型: `PROCESS_STATE`

#### 主体描述

该主体是一个令牌集。包括默认的键值对以及额外的 `pid` 键。`pid` 表示已启动进程的 `UNIX` 进程 `ID`。例如：

```ini
processname:cat groupname:cat from_state:STARTING pid:2766
```

### `PROCESS_STATE_EXITED`事件类型

表示进程已从 `RUNNING` 状态转移到 `EXITED` 状态。

- 名称: `PROCESS_STATE_EXITED`
- 子类型: `PROCESS_STATE`

#### 主体描述

该主体是一个令牌集。包括默认的键值对以及两个额外的键：`pid` 和 `expected`。`pid` 表示退出的进程的 `UNIX` 进程 `ID`。`expected` 表示进程是否以预期的退出代码退出。如果退出代码不符合预期，则为 0；如果退出代码符合预期，则为 `1`。例如：

```ini
processname:cat groupname:cat from_state:RUNNING expected:0 pid:2766
```

### `PROCESS_STATE_STOPPED`事件类型

表示进程已从 `STOPPING` 状态转移到 `STOPPED` 状态。

- 名称: `PROCESS_STATE_STOPPED`
- 子类型: `PROCESS_STATE`

#### 主体描述

该主体是一个令牌集。包括默认的键值对以及额外的 `pid` 键。`pid` 表示已启动进程的 `UNIX` 进程 `ID`。例如：

```ini
processname:cat groupname:cat from_state:STOPPING pid:2766
```

### `PROCESS_STATE_FATAL`事件类型

表示进程已从 `BACKOFF` 状态转移到 `FATAL` 状态。这意味着 `Supervisord` 尝试了 `startretries` 次以失败的方式启动该进程，并放弃了尝试重新启动。

- 名称: `PROCESS_STATE_FATAL`
- 子类型: `PROCESS_STATE`

#### 主体描述

这个事件类型是一个具有默认键值对的令牌集。例如：

```ini
processname:cat groupname:cat from_state:BACKOFF
```

### `PROCESS_STATE_UNKNOWN`事件类型

表示进程已从任何状态转移到 `UNKNOWN` 状态（表示 `supervisord` 中发生错误）。只有在 `supervisord` 本身存在编程错误时，才会发生这种状态转换。

- 名称: `PROCESS_STATE_UNKNOWN`
- 子类型: `PROCESS_STATE`

#### 主体描述

这个事件类型是一个具有默认键值对的令牌集。例如：

```ini
processname:cat groupname:cat from_state:BACKOFF
```

### `REMOTE_COMMUNICATION`事件类型

当在 `Supervisor` 的 `RPC` 接口上调用 `supervisor.sendRemoteCommEvent()` 方法时，会触发此事件类型。类型和数据是 `RPC` 方法的参数。

- 名称: `REMOTE_COMMUNICATION`
- 子类型: `EVENT`

#### 主体描述

```ini
type:type
data
```

### `PROCESS_LOG`事件类型

当进程向标准输出(stdout)或标准错误(stderr)写入内容时，会发出此事件类型。只有当文件描述符不处于捕获模式，并且 `stdout_events_enabled` 或 `stderr_events_enabled` 配置选项设置为 `true` 时，才会触发该事件。这个事件类型是抽象的，永远不会直接发送。订阅此事件类型将使订阅者接收到所有 `PROCESS_LOG` 子类型的事件通知。

- 名称: `PROCESS_LOG`
- 子类型: `EVENT`
- 主体描述: N/A

### `PROCESS_LOG_STDOUT`事件类型

表示进程已向其标准输出文件描述符写入内容。只有当文件描述符不处于捕获模式，并且 `stdout_events_enabled` 配置选项设置为 `true` 时，才会发出此事件。

- 名称: `PROCESS_LOG_STDOUT`
- 子类型: `PROCESS_LOG`

#### 主体描述

```ini
processname:name groupname:name pid:pid
data
```

### `PROCESS_LOG_STDERR`事件类型

表示进程已向其标准错误(stderr)文件描述符写入内容。只有当文件描述符不处于捕获模式，并且 `stderr_events_enabled` 配置选项设置为 `true` 时，才会发出此事件。

- 名称: `PROCESS_LOG_STDERR`
- 子类型: `PROCESS_LOG`

#### 主体描述

```ini
processname:name groupname:name pid:pid
data
```

### `PROCESS_COMMUNICATION`事件类型

当任何进程尝试在其输出中的 `<!--XSUPERVISOR:BEGIN-->` 和 `<!--XSUPERVISOR:END-->` 标记之间发送信息时，会触发此事件类型。这个事件类型是抽象的，永远不会直接发送。订阅此事件类型将使订阅者接收到所有 `PROCESS_COMMUNICATION` 子类型的事件通知。

- 名称: `PROCESS_COMMUNICATION`
- 子类型: `EVENT`
- 主体描述: N/A

### `PROCESS_COMMUNICATION_STDOUT`事件类型

表示进程已通过其标准输出(stdout)文件描述符向 `Supervisor` 发送了一条消息。

- 名称: `PROCESS_COMMUNICATION_STDOUT`
- 子类型: `PROCESS_COMMUNICATION`

#### 主体描述

```ini
processname:name groupname:name pid:pid
data
```

### `PROCESS_COMMUNICATION_STDERR`事件类型

表示进程已通过其标准错误(stderr)文件描述符向 `Supervisor` 发送了一条消息。

- 名称: `PROCESS_COMMUNICATION_STDERR`
- 子类型: `PROCESS_COMMUNICATION`

#### 主体描述

```ini
processname:name groupname:name pid:pid
data
```

### `SUPERVISOR_STATE_CHANGE`事件类型

当 `supervisord` 进程的状态发生变化时，会触发此事件类型。这个类型是抽象的，永远不会直接发送。订阅此事件类型将使订阅者接收到所有 `SUPERVISOR_STATE_CHANGE` 子类型的事件通知。

- 名称: `SUPERVISOR_STATE_CHANGE`
- 子类型: `EVENT`
- 主体描述: N/A

### `SUPERVISOR_STATE_CHANGE_RUNNING`事件类型

表示 `supervisord` 已启动。

- 名称: `SUPERVISOR_STATE_CHANGE_RUNNING`
- 子类型: `SUPERVISOR_STATE_CHANGE`
- 主体描述: Empty string

### `SUPERVISOR_STATE_CHANGE_STOPPING`事件类型

表示 `supervisord` 正在停止。

- 名称: `SUPERVISOR_STATE_CHANGE_STOPPING`
- 子类型: `SUPERVISOR_STATE_CHANGE`
- 主体描述: Empty string

### `TICK`事件类型

这是一个事件类型，可以订阅以使事件监听器每隔 `N` 秒收到 "wake-up" 通知。这个事件类型是抽象的，永远不会直接发送。订阅此事件类型将使订阅者接收到所有 `TICK` 子类型的事件通知。

请注意，下面列出的是可用的 `TICK` 事件。不能订阅任意的 `TICK` 时间间隔。如果需要一个未在下面提供的时间间隔，可以订阅以下较短的时间间隔，并在事件监听器中跟踪执行之间的时间。

- 名称: `TICK`
- 子类型: `EVENT`
- 主体描述: N/A

### `TICK_5`事件类型

这是一个可供事件监听器订阅的事件类型，以便每5秒接收到“wake-up”通知。

- 名称: `TICK_5`
- 子类型: `TICK`

#### 主体描述

该事件类型是一个令牌集，只包含一个键："when"，表示发送 tick 的时刻的时间戳（epoch time）。

```ini
when:1201063880
```

### `TICK_60`事件类型

这是一个可供事件监听器订阅的事件类型，以便每60秒接收到“wake-up”通知。

- 名称: `TICK_60`
- 子类型: `TICK`

#### 主体描述

该事件类型是一个令牌集，只包含一个键："when"，表示发送 tick 的时刻的时间戳（epoch time）。

```ini
when:1201063880
```

### `TICK_3600`事件类型

这是一个可供事件监听器订阅的事件类型，以便每3600秒（1小时）接收到“wake-up”通知。

- 名称: `TICK_3600`
- 子类型: `TICK`

#### 主体描述

该事件类型是一个令牌集，只包含一个键："when"，表示发送 tick 的时刻的时间戳（epoch time）。

```ini
when:1201063880
```

### `PROCESS_GROUP`事件类型

当进程组被添加到或从 `Supervisor` 中移除时，会触发此事件类型。这个类型是抽象的，永远不会直接发送。订阅此事件类型将使订阅者接收到所有 `PROCESS_GROUP` 子类型的事件通知。

- 名称: `PROCESS_GROUP`
- 子类型: `EVENT`
- 主体描述: N/A

### `PROCESS_GROUP_ADDED`事件类型

表示进程组已添加到 Supervisor 的配置中。

- 名称: `PROCESS_GROUP_ADDED`
- 子类型: `PROCESS_GROUP`
- 主体描述: 该主体是一个仅包含 `groupname` 键值对的令牌集。

```ini
groupname:cat
```

### `PROCESS_GROUP_REMOVED`事件类型

表示进程组已从 `Supervisor` 的配置中移除。

- 名称: `PROCESS_GROUP_REMOVED`
- 子类型: `PROCESS_GROUP`
- 主体描述: 该主体是一个仅包含 `groupname` 键值对的令牌集。