---
title: 配置文件
order: 4
---

Supervisor配置文件通常被命名为 `supervisord.conf`。被 `supervisord` 和 `supervisorctl` 两个应用程序共同使用。如果没有使用 `-c` 选项（该选项用于显式告知应用程序配置文件的名称）启动任一应用程序，该应用程序将按照指定的顺序在以下位置查找名为 `supervisord.conf` 的文件，并使用找到的第一个文件。

1. `../etc/supervisord.conf` (相对于可执行文件)

2. `../supervisord.conf` (相对于可执行文件)

3. `$CWD/supervisord.conf`

4. `$CWD/etc/supervisord.conf`

5. `/etc/supervisord.conf`

6. `/etc/supervisor/supervisord.conf` (自Supervisor 3.3.0版本起)

:::info{title=提示}
为 Debian 和 Ubuntu 打包的 Supervisor 版本包含一个补丁，将 `/etc/supervisor/supervisord.conf` 添加到搜索路径中。首个包含该补丁的Supervisor PyPI软件包是 Supervisor 3.3.0。
:::

## 文件格式

`supervisord.conf `是一个 `Windows-INI` 风格（Python ConfigParser）的文件。包含多个部分（每个由 `[header]` 表示），以及在各个部分内的键值对。下面描述了各个部分及其允许的值。

### 环境变量

在启动 `supervisord` 时存在于环境中的环境变量可以使用Python字符串表达式语法 `%(ENV_X)s` 在配置文件中使用：

```ini
[program:example]
command=/usr/bin/example --loglevel=%(ENV_LOGLEVEL)s
```

在上面的示例中，表达式 `%(ENV_LOGLEVEL)s` 将会展开为环境变量 `LOGLEVEL` 的值。

:::info{title=提示}
在Supervisor 3.2及更高版本中，所有选项都支持 `%(ENV_X)s` 表达式。在之前的版本中，一些选项支持这种表达式，但大多数不支持。请参阅下面每个选项的文档以获取详细信息。
:::

## `[unix_http_server]`设置

`supervisord.conf` 文件包含一个名为 `[unix_http_server]` 的部分，其中应插入监听UNIX Socket的HTTP服务器的配置参数。如果配置文件中没有 `[unix_http_server]` 部分，则不会启动UNIX Socket的HTTP服务器。允许的配置值如下所示。

### `[unix_http_server]`值

`file`

UNIX Socket的路径，supervisor将在其上监听 `HTTP/XML-RPC` 请求。supervisorctl使用 `XML-RPC` 通过该端口与supervisord进行通信。此选项可以包含 `%(here)s` 值，会展开为找到supervisord配置文件的目录。

- 默认值：无。
- 必需项：否。
- 引入版本：3.0。

:::warning{title=注意}
`echo_supervisord_conf` 输出的示例配置使用 `/tmp/supervisor.sock` 作为Socket文件。该路径仅作为示例，很可能需要将其更改为适合系统的位置。某些系统会定期删除 `/tmp` 目录中的旧文件。如果Socket文件被删除，supervisorctl将无法连接到supervisord。
:::

`chmod`

在启动时更改UNIX Socket的UNIX权限模式位为此值。

- 默认值：`0700`。
- 必需项：否。
- 引入版本：3.0。

`chown`

将Socket文件的用户和组更改为此值。可以是UNIX用户名（例如chrism）或由冒号分隔的UNIX用户名和组（例如 `chrism:wheel`）。

- 默认值：使用启动supervisord的用户的用户名和组。
- 必需项：否。
- 引入版本：3.0。

`username`

用于对此HTTP服务器进行身份验证所需的用户名。

- 默认值：不需要用户名。
- 必需项：否。
- 引入版本：3.0。

`password`

用于对此HTTP服务器进行身份验证所需的密码。可以是明文密码，或者可以在字符串 `{SHA}` 前面以SHA-1哈希值的形式进行指定。例如，`{SHA}82ab876d1387bfafe46cc1c8a2ef074eae50cb1d` 是密码“thepassword”的SHA存储版本。

请注意，哈希密码必须以十六进制格式表示。

- 默认值：不需要密码。
- 必需项：否。
- 引入版本：3.0。

### `[unix_http_server]`示例

```ini
[unix_http_server]
file = /tmp/supervisor.sock
chmod = 0777
chown= nobody:nogroup
username = user
password = 123
```

## `[inet_http_server]`设置

`supervisord.conf` 文件包含一个名为 `[inet_http_server]` 的部分，其中应插入监听TCP（互联网）Socket的HTTP服务器的配置参数。如果配置文件中没有 `[inet_http_server]` 部分，则不会启动inet HTTP服务器。允许的配置值如下所示。

:::warning{title=注意}
默认情况下，inet HTTP服务器未启用。如果选择启用，请阅读以下安全警告。inet HTTP服务器仅适用于可信环境中。应该仅绑定到localhost或仅从隔离的、受信任的网络内部访问。inet HTTP服务器不支持任何形式的加密。inet HTTP服务器默认不使用身份验证（参见 `username=` 和 `password=` 选项）。可以通过supervisorctl远程控制inet HTTP服务器。另外还提供一个Web界面，允许启动或停止子进程，并查看子进程日志。切勿将inet HTTP服务器暴露在公共互联网上。
:::

### `[inet_http_server]`值

`port`

supervisor将在指定的TCP主机:端口（例如 `127.0.0.1:9001` ）上监听 `HTTP/XML-RPC` 请求。supervisorctl将使用 `XML-RPC` 通过该端口与supervisord进行通信。要监听机器上的所有接口，请使用 `:9001` 或 `*:9001`。请阅读上面的安全警告。

- 默认值：无。
- 必需项：是。
- 引入版本：3.0。

`username`

用于对此HTTP服务器进行身份验证所需的用户名。

- 默认值：不需要用户名。
- 必需项：否。
- 引入版本：3.0。

`password`

用于对此HTTP服务器进行身份验证所需的密码。可以是明文密码，或者可以在字符串 `{SHA}` 前面以SHA-1哈希值的形式进行指定。例如，`{SHA}82ab876d1387bfafe46cc1c8a2ef074eae50cb1d` 是密码“thepassword”的SHA存储版本。

请注意，哈希密码必须以十六进制格式表示。

- 默认值：不需要密码。
- 必需项：否。
- 引入版本：3.0。

### `[inet_http_server]`示例

```ini
[inet_http_server]
port = 127.0.0.1:9001
username = user
password = 123
```

## `[supervisord]`设置

`supervisord.conf` 文件包含一个名为 `[supervisord]` 的部分，其中应插入与supervisord进程相关的全局设置。以下是相关设置。

### `[supervisord]`值

`logfile`

supervisord进程的活动日志路径。此选项可以包含 `%(here)s` 值，该值会展开为找到supervisord配置文件的目录。

:::info{title=提示}
如果将日志文件(logfile)设置为像 `/dev/stdout` 这样的特殊文件，该文件不支持随机访问，必须通过设置 `logfile_maxbytes = 0` 来禁用日志回滚。
:::

- 默认值：`$CWD/supervisord.log`。
- 必需项：否。
- 引入版本：3.0。

`logfile_maxbytes`

活动日志文件在进行轮转之前可以占用的最大字节数（可以在值中使用后缀计数器，如“KB”、“MB”和“GB”）。将此值设为0表示无限制的日志大小。

- 默认值：50MB。
- 必需项：否。
- 引入版本：3.0。

`logfile_backups`

由于活动日志文件回滚而保留的备份数量。如果设置为0，则不会保留任何备份。

- 默认值：10。
- 必需项：否。
- 引入版本：3.0。

`loglevel`

日志级别，决定写入supervisord活动日志的内容。可选值为 `critical`、`error`、`warn`、`info`、`debug`、`trace`或 `blather`。请注意，在调试（`debug`）日志级别下，supervisord日志文件将记录其子进程的stderr/stdout输出以及有关进程状态变化的详细信息，这对于调试未能正确启动的进程非常有用。另请参阅：活动日志级别。

- 默认值：info。
- 必需项：否。
- 引入版本：3.0。

`pidfile`

supervisord保持其pid文件的位置。此选项可以包含 `%(here)s` 值，该值会展开为找到supervisord配置文件的目录。

- 默认值：`$CWD/supervisord.pid`。
- 必需项：否。
- 引入版本：3.0。

`umask`

supervisord进程的umask。

- 默认值：022。
- 必需项：否。
- 引入版本：3.0。

`nodaemon`

如果设置为true，supervisord将以前台模式启动，而不是作为守护进程。

- 默认值：false。
- 必需项：否。
- 引入版本：3.0。

`silent`

如果设置为true且未作为守护进程运行，日志将不会输出到标准输出(stdout)。

- 默认值：false。
- 必需项：否。
- 引入版本：4.2.0。

`minfds`

在supervisord成功启动之前，必须可用的最小文件描述符数量。将调用setrlimit尝试提高supervisord进程的软限制和硬限制以满足 `minfds`。只有以root身份运行supervisord时，才能提高硬限制。supervisord使用文件描述符频繁，在无法从操作系统获取文件描述符时会进入故障模式，因此指定最小值以确保在执行过程中不会耗尽文件描述符非常有用。这些限制将被托管子进程继承。在Solaris上，这个选项特别有用，因为默认情况下每个进程的fd限制较低。

- 默认值：1024。
- 必需项：否。
- 引入版本：3.0。

`minprocs`

在supervisord成功启动之前，必须可用的最小进程描述符数量。将调用setrlimit尝试提高supervisord进程的软限制和硬限制以满足 `minprocs`。只有以root身份运行supervisord时，才能提高硬限制。当操作系统的进程描述符耗尽时，supervisord将进入故障模式，因此确保在supervisord启动时有足够的进程描述符可用非常重要。

- 默认值：200。
- 必需项：否。
- 引入版本：3.0。

`nocleanup`

防止supervisord在启动时清除任何现有的 `AUTO` 子进程日志文件。这对于调试非常有用。

- 默认值：false。
- 必需项：否。
- 引入版本：3.0。

`childlogdir`

用于AUTO子进程日志文件的目录。此选项可以包含 `%(here)s` 值，该值会展开为找到supervisord配置文件的目录。

- 默认值：Python的 `tempfile.gettempdir()` 函数返回的值。
- 必需项：否。
- 引入版本：3.0。

`user`

在进行任何有意义的处理之前，指示supervisord切换到此UNIX用户帐户。只有以root用户身份启动supervisord时才能进行用户切换。

- 默认值：不切换用户。
- 必需项：否。
- 引入版本：3.0。

更改：3.3.4版本。如果supervisord无法切换到指定的用户，将向 `stderr` 写入错误消息，然后立即退出。在早期版本中，会继续运行，但会记录一条 `critical` 级别的消息。

`directory`

当supervisord变为守护进程时，切换到此目录。此选项可以包含 `%(here)s` 值，该值会展开为找到supervisord配置文件的目录。

- 默认值：不更改当前目录。
- 必需项：否。
- 引入版本：3.0。

`strip_ansi`

从子进程日志文件中去除所有的ANSI转义序列。

- 默认值：false。
- 必需项：否。
- 引入版本：3.0。

`environment`

以 `KEY="val""，"KEY2="val2"` 的形式的键值对列表，将放置在所有子进程的环境中。这不会改变supervisord本身的环境。此选项可以包含 `%(here)s` 值，该值会展开为找到supervisord配置文件的目录。包含非字母数字字符的值应该被引用（例如KEY="val:123"，KEY2="val,456"）。否则，值的引用是可选的，但建议使用。要转义百分号字符，只需使用两个（例如 `URI="/first%%20name"`）。请注意，子进程将继承用于启动supervisord的Shell的环境变量，除非在此处覆盖或在程序的 `environment` 选项中。请参阅[子进程环境](http://supervisord.org/subprocess.html#subprocess-environment)。

- 默认值：无。
- 必需项：否。
- 引入版本：3.0。

`identifier`

此supervisor进程的标识符字符串，由RPC接口使用。

- 默认值：supervisor。
- 必需项：否。
- 引入版本：3.0。

### `[supervisord]`示例

```ini
[supervisord]
logfile = /tmp/supervisord.log
logfile_maxbytes = 50MB
logfile_backups=10
loglevel = info
pidfile = /tmp/supervisord.pid
nodaemon = false
minfds = 1024
minprocs = 200
umask = 022
user = chrism
identifier = supervisor
directory = /tmp
nocleanup = true
childlogdir = /tmp
strip_ansi = false
environment = KEY1="value1",KEY2="value2"
```

## `[supervisorctl]`设置

### `[supervisorctl]`值

`serverurl`

用于访问supervisord服务器的URL，例如 `http://localhost:9001`。对于UNIX Socket，请使用 `unix:///absolute/path/to/file.sock`。

- 默认值：`http://localhost:9001`。
- 必需项：否。
- 引入版本：3.0。

`username`

要传递给supervisord服务器以进行身份验证的用户名。这应该与尝试访问的端口或UNIX Socket的supervisord服务器配置中的用户名相同。

- 默认值：无。
- 必需项：否。
- 引入版本：3.0。

`password`

要传递给supervisord服务器以进行身份验证的密码。这应该是从尝试访问的端口或UNIX Socket的supervisord服务器配置中获取的明文密码。此值不能作为SHA哈希传递。与文件中指定的其他密码不同，必须以明文形式提供。

- 默认值：无。
- 必需项：否。
- 引入版本：3.0。

`prompt`

作为 supervisorctl 提示符使用的字符串。

- 默认值：`supervisor`。
- 必需项：否。
- 引入版本：3.0。

`history_file`

用作 `readline` 持久历史文件的路径。如果通过选择一个路径启用了此功能，`supervisorctl` 命令将保存在该文件中，并且可以使用 `readline`（例如，向上箭头）来调用在上次 `supervisorctl` 会话中执行过的命令。

- 默认值：无。
- 必需项：否。
- 引入版本：3.0a5。

### `[supervisorctl]`示例

```ini
[supervisorctl]
serverurl = unix:///tmp/supervisor.sock
username = chris
password = 123
prompt = mysupervisor
```

## `[program:x]`设置

配置文件必须包含一个或多个程序段，以便 `supervisord` 知道应该启动和控制哪些程序。头部值是复合值。单词是 "program"，紧接着一个冒号，然后是程序名称。`[program:foo]` 的头部值描述了一个名为 "foo" 的程序。该名称在控制由此配置创建的进程的客户端应用程序中使用。创建一个没有名称的程序段是错误的。名称不能包含冒号字符或括号字符。在其他指定位置，名称的值被用作 `%(program_name)s` 字符串表达式展开的值。

:::info{title=示例}
一个 `[program:x]` 段实际上在 `supervisor`（从 3.0 版本开始）中代表着一个“同质进程组”。该组的成员由配置中 `numprocs` 和 `process_name` 参数的组合定义。默认情况下，如果 `numprocs` 和 `process_name` 保持不变，默认值不变，则由 `[program:x]` 表示的组将被命名为 `x`，并且其中只有一个名为 `x` 的进程。这提供了与旧版 `supervisor` 不同的一定程度的向后兼容性，旧版 `supervisor` 并未将程序段视为同质进程组定义。

举例来说，如果你有一个 `[numprocs:foo]` 段，其中 `numprocs` 为3，`process_name` 表达式为 `%(program_name)s_%(process_num)02d`，那么"foo"组将包含三个进程，分别命名为 `foo_00`、`foo_01` 和 `foo_02`。这使得可以使用单个 `[program:x]` 段启动多个非常相似的进程。所有日志文件名、所有环境变量字符串以及程序的命令也可以包含类似的Python字符串表达式，以便为每个进程传递略有不同的参数。
:::

### `[program:x]`值

`command`

当启动该程序时将运行的命令。命令可以是绝对路径（例如 `/path/to/programname`）或相对路径（例如 programname）。如果是相对路径，`supervisord` 的环境变量 `$PATH` 将被搜索以找到可执行文件。程序可以接受参数，例如 `/path/to/program foo bar`。命令行可以使用双引号将带有空格的参数分组，以传递给程序，例如 `/path/to/program/name -p "foo bar"`。注意，`command` 的值可以包含 Python 字符串表达式，例如 `/path/to/programname --port=80%(process_num)02d` 可以在运行时展开为 `/path/to/programname --port=8000`。字符串表达式根据字典进行求值，该字典包含 `group_name`、`host_node_name`、`program_name`、`process_num`、`numprocs`、`here`（`supervisord` 配置文件所在的目录）以及所有以 ENV_ 为前缀的 `supervisord` 环境变量。受控程序本身不应该是守护进程，因为 `supervisord` 假设负责守护化其子进程（参见[子进程的非守护化](http://supervisord.org/subprocess.html#nondaemonizing-of-subprocesses)）。

:::info{title=提示}
如果命令看起来像配置文件注释，例如 `command=bash -c 'foo ; bar'`，则该命令将被截断为 `command=bash -c 'foo'`。引用也无法阻止此行为，因为配置文件读取器不会像 shell 那样解析命令。
:::

- 默认值：无。
- 必需项：否。
- 引入版本：3.0。

更改：4.2.0。添加了对 `numprocs` 展开的支持。

`process_name`

用于构造该进程的 `supervisor` 进程名称的 Python 字符串表达式。通常情况下，除非改变了 `numprocs`，否则不需要担心设置。该字符串表达式将根据一个字典进行求值，该字典包含 `group_name`、`host_node_name`、`process_num`、`program_name` 和 `here`（supervisord 配置文件所在的目录）。

- 默认值：`%(program_name)s`。
- 必需项：否。
- 引入版本：3.0。

`numprocs`

Supervisor 将启动由 `numprocs` 指定的该程序的多个实例。请注意，如果 `numprocs > 1`，则 `process_name` 表达式必须在其中包含 `%(process_num)s`（或者任何其他有效的包含 `process_num` 的 Python 字符串表达式）。

- 默认值：1。
- 必需项：否。
- 引入版本：3.0。

`numprocs_start`

一个整数偏移量，用于计算 `process_num` 的起始编号。

- 默认值：0。
- 必需项：否。
- 引入版本：3.0。

`priority`

程序在启动和关闭顺序中的相对优先级。较低的优先级表示程序在启动时首先启动，关闭时最后关闭，并且在各个客户端使用聚合命令（例如 `start all` / `stop all`）时也是如此。较高的优先级表示程序在启动时最后启动，关闭时首先关闭。

- 默认值：999。
- 必需项：否。
- 引入版本：3.0。

`autostart`

如果设置为 true，当 supervisord 启动时，该程序将自动启动。

- 默认值：true。
- 必需项：否。
- 引入版本：3.0。

`startsecs`

程序在启动后需要保持运行的总秒数，以确保启动成功（将进程从 `STARTING` 状态转换为 `RUNNING` 状态）。设置为 `0` 表示程序不需要保持运行任何特定的时间。

:::info{title=提示}
即使进程以“预期”的退出代码（参见 `exitcodes`）退出，如果进程在 `startsecs` 之前退出得更快，启动仍将被视为失败。
:::

- 默认值：1。
- 必需项：否。
- 引入版本：3.0。

`startretries`

在放弃并将进程置于 `FATAL` 状态之前，supervisord 在尝试启动程序时允许的连续失败尝试次数。

:::info{title=提示}
在每次重新启动失败后，进程将置于 `BACKOFF` 状态，并且每次重试尝试将需要越来越长的时间。

请参阅[进程状态](http://supervisord.org/subprocess.html#process-states)以了解有关 `FATAL` 和 `BACKOFF` 状态的解释。
:::

- 默认值：3。
- 必需项：否。
- 引入版本：3.0。

`autorestart`

指定如果进程在 `RUNNING` 状态时退出，`supervisord` 是否应自动重新启动该进程。可能的取值为 `false`、`unexpected` 或 `true`。如果设置为 `false`，则不会自动重新启动该进程。如果设置为 `unexpected`，则当程序以与该进程配置关联的退出代码（参见 `exitcodes`）之外的退出代码退出时，将重新启动该进程。如果设置为 `true`，则无条件地在进程退出时重新启动，而不考虑其退出代码。

:::info{title=提示}
`autorestart` 控制着 `supervisord` 是否在程序成功启动后（进程处于 `RUNNING` 状态）退出时自动重新启动该程序。

对于进程启动过程中（进程处于 `STARTING` 状态），`supervisord` 有一个不同的重启机制。在进程启动过程中的重试次数由 `startsecs` 和 `startretries` 控制。
:::

- 默认值：unexpected。
- 必需项：否。
- 引入版本：3.0。

`exitcodes`

定义了用于 `autorestart` 的该程序的“预期”退出代码列表。如果 `autorestart` 参数设置为 `unexpected`，并且进程以除了由 `supervisor` 停止请求引起的方式之外的任何其他方式退出，`supervisord` 将在进程以未在此列表中定义的退出代码退出时重新启动该进程。

- 默认值：0。
- 必需项：否。
- 引入版本：3.0。

:::info{title=提示}
在 supervisor 4.0 之前的版本中，默认值为 `0,2`。在 supervisor 4.0 中，将默认值更改为 `0`。
:::

`stopsignal`

在请求停止时用于终止程序的信号。可以使用信号的名称或数字来指定。通常情况下，可选的信号包括：`TERM`、`HUP`、`INT`、`QUIT`、`KILL`、`USR1` 或 `USR2`。

- 默认值：TERM。
- 必需项：否。
- 引入版本：3.0。

`stopwaitsecs`

在程序被发送停止信号后，等待操作系统返回 `SIGCHLD` 给 `supervisord` 的时间（以秒为单位）。如果在这段时间内 `supervisord` 未接收到进程的 `SIGCHLD` 信号，`supervisord` 将尝试使用最终的 `SIGKILL` 杀死该进程。

- 默认值：10。
- 必需项：否。
- 引入版本：3.0。

`stopasgroup`

如果设置为 `true`，则该标志会导致 `supervisord` 向整个进程组发送停止信号，并且意味着 `killasgroup` 也设置为 `true`。这对于某些程序非常有用，比如以调试模式运行的 Flask，不会将停止信号传播给子进程，导致子进程成为孤立进程。

- 默认值：false。
- 必需项：否。
- 引入版本：3.0b1。

`killasgroup`

如果设置为 `true`，则在通过发送 `SIGKILL` 给程序终止时，将发送给整个进程组，同时也会处理其子进程，这对于使用 `multiprocessing` 的 Python 程序非常有用。

- 默认值：false。
- 必需项：否。
- 引入版本：3.0a11。

`user`

指示 `supervisord` 使用此 `UNIX` 用户帐户作为运行该程序的帐户。只有在以 `root` 用户身份运行 `supervisord` 时，才能切换用户。如果 `supervisord` 无法切换到指定的用户，则该程序将不会启动。

:::info{title=提示}
用户将仅通过使用 `setuid` 进行更改。这不会启动登录 shell，也不会更改像 `USER` 或 `HOME` 这样的环境变量。有关详细信息，请参阅[子进程环境](http://supervisord.org/subprocess.html#subprocess-environment)。
:::

- 默认值：不切换用户。
- 必需项：否。
- 引入版本：3.0。

`redirect_stderr`

如果设置为 `true`，则导致进程的 `stderr` 输出回送到 `supervisord` 的 `stdout` 文件描述符（在 UNIX shell 命令中，这相当于执行 `/the/program 2>&1`）。

:::info{title=提示}
在 `[eventlistener:x]` 部分中不要设置 `redirect_stderr=true`。事件监听器使用 `stdout` 和 `stdin` 与 `supervisord` 进行通信。如果重定向 `stderr`，`stderr` 的输出将干扰事件监听器协议。
:::

- 默认值：false。
- 必需项：否。
- 引入版本：3.0，取代了2.0版本的 `log_stdout` 和 `log_stderr`。

`stdout_logfile`

将进程的 `stdout` 输出放入此文件中（如果 `redirect_stderr` 为 true，则也将 `stderr` 输出放入此文件）。如果未设置 `stdout_logfile` 或将其设置为 `AUTO`，`supervisord` 将自动选择一个文件位置。如果设置为 NONE，则 `supervisord` 将不创建日志文件。AUTO 日志文件及其备份将在 `supervisord` 重新启动时删除。`stdout_logfile` 的值可以包含 Python 字符串表达式，这些表达式将根据一个字典进行求值，该字典包含 `group_name`、`host_node_name`、`process_num`、`program_name` 和 `here`（`supervisord` 配置文件所在的目录）的键。

:::info{title=提示}
在启用日志文件回滚（`stdout_logfile_maxbytes`）时，两个进程无法共享一个单独的日志文件（`stdout_logfile`）。将会导致文件损坏。
:::

:::info{title=提示}
如果 `stdout_logfile` 设置为像 `/dev/stdout` 这样的特殊文件，该文件不可寻址，则必须通过设置 `stdout_logfile_maxbytes = 0` 来禁用日志回滚。
:::

- 默认值：`AUTO`。
- 必需项：否。
- 引入版本：3.0，取代了2.0版本的 `logfile`。

`stdout_logfile_maxbytes`

`stdout_logfile` 在进行轮转之前可以使用的最大字节数（可以在值中使用后缀乘数，如 "KB"、"MB" 和 "GB"）。将此值设置为 0 表示无限制的日志大小。

- 默认值：50MB。
- 必需项：否。
- 引入版本：3.0，取代了2.0版本的 `logfile_maxbytes`。

`stdout_logfile_backups`

由于进程的 `stdout` 日志文件回滚而保留的 `stdout_logfile` 备份数量。如果设置为 `0`，则不会保留任何备份。

- 默认值：10。
- 必需项：否。
- 引入版本：3.0，取代了2.0版本的 `logfile_backups`。

`stdout_capture_maxbytes`

进程处于“stdout捕获模式”时写入捕获FIFO的最大字节数（参见[捕获模式](http://supervisord.org/logging.html#capture-mode)）。应为整数（可以在值中使用后缀计数器，如“KB”、“MB”和“GB”）。如果此值为0，则进程捕获模式将关闭。

- 默认值：0。
- 必需项：否。
- 引入版本：3.0。

`stdout_syslog`

如果设置为true，stdout将与进程名称一起被重定向到syslog。

- 默认值：false。
- 必需项：否。
- 引入版本：4.0。

`stderr_logfile`

将进程的stderr输出放入此文件中，除非 `redirect_stderr` 设置为true。接受与 `stdout_logfile` 相同的值类型，并可以包含相同的Python字符串表达式。

:::info{title=提示}
当启用日志回滚（`stderr_logfile_maxbytes`）时，两个进程无法共享同一个日志文件（`stderr_logfile`），这会导致文件损坏。
:::

:::info{title=提示}
如果将 `stderr_logfile` 设置为像 `/dev/stderr` 这样的特殊文件，该文件不支持随机访问，则需要通过将 `stderr_logfile_maxbytes` 设置为0来禁用日志回滚。
:::

- 默认值：`AUTO`。
- 必需项：否。
- 引入版本：3.0。

`stderr_logfile_maxbytes`

`stderr_logfile` 的日志文件回滚前的最大字节数。接受与 `stdout_logfile_maxbytes` 相同的值类型。

- 默认值：50MB。
- 必需项：否。
- 引入版本：3.0。

`stderr_logfile_backups`

由于进程的 `stderr` 日志文件回滚而保留的备份数量。如果设置为 `0`，则不会保留任何备份。

- 默认值：10。
- 必需项：否。
- 引入版本：3.0。

`stderr_capture_maxbytes`

进程处于“stderr捕获模式”时写入捕获FIFO的最大字节数（参见捕获模式）。应为整数（可以在值中使用后缀计数器，如“KB”、“MB”和“GB”）。如果此值为0，则进程捕获模式将关闭。

- 默认值：0。
- 必需项：否。
- 引入版本：3.0。

`stderr_events_enabled`

如果设置为true，当进程向其stderr文件描述符写入内容时，将触发PROCESS_LOG_STDERR事件。只有在接收数据时文件描述符不处于捕获模式（参见[捕获模式](http://supervisord.org/logging.html#capture-mode)）时，才会触发这些事件。

- 默认值：false。
- 必需项：否。
- 引入版本：3.0a7。

`stderr_syslog`

如果设置为true，stderr将与进程名称一起被重定向到syslog。

- 默认值：false。
- 必需项：否。
- 引入版本：4.0.0。

`environment`

一个以 `KEY="val""，"KEY2="val2"` 形式的键值对列表，将放置在子进程的环境中。环境字符串可以包含Python字符串表达式，这些表达式将根据包含 `group_name`、`host_node_name`、`process_num`、`program_name` 和 `here`（supervisord配置文件的目录）的字典进行求值。包含非字母数字字符的值应该被引用（例如 `KEY="val:123"，KEY2="val,456"`）。否则，引用值是可选的，但建议使用。请注意，子进程将继承用于启动“supervisord”的Shell的环境变量，除非在此处覆盖。请参阅[子进程环境](http://supervisord.org/subprocess.html#subprocess-environment)。

- 默认值：没有额外的环境设置。
- 必需项：否。
- 引入版本：3.0。

`directory`

一个文件路径，表示supervisord在执行子进程之前应该临时切换到的目录。

- 默认值：不切换目录（继承supervisor的目录）。
- 必需项：否。
- 引入版本：4.0.0。

`umask`

一个八进制数（例如002、022），表示进程的umask。

- 默认值：没有特殊的umask（继承supervisor的umask）。
- 必需项：否。
- 引入版本：3.0。

`serverurl`

将URL以 `SUPERVISOR_SERVER_URL`（参见 `supervisor.childutils`）的形式传递给子进程的环境，以便子进程能够轻松与内部HTTP服务器进行通信。如果提供了此选项，应具有与同名的 `[supervisorctl]` 部分选项相同的语法和结构。如果设置为AUTO或未设置，则supervisor将自动构建一个服务器URL，优先选择监听UNIX Socket的服务器而不是监听InternetSocket的服务器。

- 默认值：AUTO。
- 必需项：否。
- 引入版本：3.0。

### `[program:x]`示例

```ini
[program:cat]
command=/bin/cat
process_name=%(program_name)s
numprocs=1
directory=/tmp
umask=022
priority=999
autostart=true
autorestart=unexpected
startsecs=10
startretries=3
exitcodes=0
stopsignal=TERM
stopwaitsecs=10
stopasgroup=false
killasgroup=false
user=chrism
redirect_stderr=false
stdout_logfile=/a/path
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stdout_capture_maxbytes=1MB
stdout_events_enabled=false
stderr_logfile=/a/path
stderr_logfile_maxbytes=1MB
stderr_logfile_backups=10
stderr_capture_maxbytes=1MB
stderr_events_enabled=false
environment=A="1",B="2"
serverurl=AUTO
```

## `[include]`设置

`supervisord.conf` 文件可以包含一个名为 `[include]` 的部分。如果配置文件包含 `[include]` 部分，则该部分必须包含一个名为“files”的键。此键中的值指定要在配置中包含的其他配置文件。

:::info{title=提示}
`[include]` 部分只由 `supervisord` 处理，而被 `supervisorctl` 忽略。
:::

### `[include]`值

`files`

一个由空格分隔的文件glob序列。每个文件glob可以是绝对路径或相对路径。如果文件glob是相对路径，则被视为相对于包含其配置文件所在的位置。"glob"是一个文件模式，根据Unix shell使用的规则与指定的模式匹配。不进行波浪线扩展（tilde expansion），但 `*`、`?` 和用 `[]` 表示的字符范围将正确匹配。字符串表达式根据包含 `host_node_name` 和 `here`（supervisord配置文件所在的目录）的字典进行求值。不支持从已包含文件中进行递归包含。

- 默认值：无。
- 必需项：是。
- 引入版本：3.0。

变更：3.3.0。新增对 `host_node_name` 扩展的支持。

### `[include]`示例

```ini
[include]
files = /an/absolute/filename.conf /an/absolute/*.conf foo.conf config??.conf
```

## `[group:x]`设置

将“homogeneous”的进程组（也称为“programs”）组合成一个“heterogeneous”的进程组通常很有用，这样可以作为一个单元从Supervisor的各种控制接口进行控制。

为了将程序放置到一个组中，以便可以作为一个单元进行处理，在配置文件中定义 `[group:x]` 部分。组头值是一个复合值。由单词“group”直接跟着冒号，然后是组名组成。`[group:foo]` 的头值描述了一个名为“foo”的组。该名称在控制作为此配置结果而创建的进程的客户端应用程序中使用。创建没有名称的 `group` 部分是错误的。名称不能包含冒号字符或方括号字符。

对于 `[group:x]`，在配置文件的其他地方必须有一个或多个 `[program:x]` 部分，并且组必须通过名称在 `programs` 值中引用。

如果将“homogeneous”的进程组（由程序部分表示）通过 `[group:x]` 部分的 `programs` 行放置到一个“heterogeneous”组中，那么在supervisor的运行时环境中，并不会存在由程序部分隐含的同质组。相反，属于每个同质组的所有进程都将放置在异质组中。例如，考虑以下组配置：

```ini
[group:foo]
programs=bar,baz
priority=999
```

根据上述配置，在supervisord启动时，`bar` 和 `baz` 的同质组将不存在，并且原本的进程将被移动到 `foo` 组中。

### `[group:x]`值

`programs`

一个由逗号分隔的程序名称列表。列出的程序将成为该组的成员。

- 默认值：无。
- 必需项：是。
- 引入版本：3.0。

`priority`

一个与 `[group:x]` 中分配给组的 `[program:x]` 优先级值类似的优先级数字。

- 默认值：999。
- 必需项：否。
- 引入版本：3.0。

### `[group:x]`示例

```ini
[group:foo]
programs=bar,baz
priority=999
```

## `[fcgi-program:x]`设置

Supervisor可以管理一组监听同一Socket的 [FastCGI](http://www.fastcgi.com/) 进程。在此之前，对于FastCGI的部署灵活性有所限制。为了完全管理进程，可以在Apache下使用mod_fastcgi，但那时将受限于Apache的低效并发模型，即每个连接一个进程或线程。除了需要更多的CPU和内存资源外，每个连接一个进程/线程的模型可能会被慢速资源迅速饱和，从而阻止其他资源的服务。为了利用像lighttpd或nginx这样不包含内置进程管理器的新型事件驱动Web服务器，必须使用诸如cgi-fcgi或spawn-fcgi之类的脚本。这些脚本可以与supervisord或daemontools等进程管理器结合使用，但需要每个FastCGI子进程绑定到自己的Socket。这种方法的缺点包括：不必要复杂的Web服务器配置、不友好的重启以及降低的容错性。如果能够共享Socket，那么当FastCGI进程组可以共享Socket时，Web服务器配置将更简洁，因为需要配置的Socket数量较少。共享Socket允许进行优雅的重启，因为在任何子进程重新启动时，Socket仍由父进程保持绑定。最后，共享Socket具有更高的容错性，因为如果某个进程失败，其他进程仍可以继续处理入站连接。

有了集成的FastCGI生成支持，Supervisor提供了两者兼顾的优势。既可以获得具备完整功能的进程管理，又可以使一组FastCGI进程共享Socket，而不局限于特定的Web服务器。这是一种清晰的责任分离，使得Web服务器和进程管理器各司其职，发挥各自的优势。

:::info{title=提示}
Supervisor中的Socket管理器最初是为了支持FastCGI进程而开发的，但并不限于FastCGI。其他协议也可以与之一起使用，无需特殊配置。任何能够从文件描述符访问打开Socket的程序（例如，在Python中使用 [socket.fromfd](http://docs.python.org/library/socket.html#socket.fromfd)）都可以使用Socket管理器。Supervisor会在分组中的第一个子进程分叉之前自动创建Socket、绑定和监听。该Socket将通过文件描述符号 `0`（零）传递给每个子进程。当分组中的最后一个子进程退出时，Supervisor会关闭该Socket。
:::

:::info{title=提示}
在Supervisor 3.4.0之前，FastCGI程序（`[fcgi-program:x]`）无法在组（`[group:x]`）中进行引用。
:::

所有可用于 `[program:x]` 部分的选项也适用于 `fcgi-program` 部分。

### `[fcgi-program:x]`值

`[fcgi-program:x]` 部分拥有一些 `[program:x]` 部分没有的键值。

`socket`

该程序的FastCGISocket，可以是TCP或UNIX Socket。对于TCPSocket，请使用以下格式：`tcp://localhost:9002`。对于UNIX Socket，请使用 `unix:///absolute/path/to/file.sock`。字符串表达式会根据一个包含"program_name"和"here"（supervisord配置文件所在的目录）键的字典进行求值。

- 默认值：无。
- 必需项：是。
- 引入版本：3.0。

`socket_backlog`

设置 socket 的监听（listen(2)）队列长度。

- 默认值：socket.SOMAXCONN。
- 必需项：否。
- 引入版本：3.4.0。

`socket_owner`

对于UNIX socket，该参数可以用于指定FastCGI socket的用户和组。可以是一个UNIX用户名（例如chrism），也可以是由冒号分隔的UNIX用户名和组（例如chrism:wheel）。

- 默认值：使用为fcgi-program设置的用户和组。
- 必需项：否。
- 引入版本：3.4.0。

`socket_mode`

对于UNIX socket，该参数可用于指定权限模式。

- 默认值：0700。
- 必需项：否。
- 引入版本：3.4.0。

请参考 [[program:x]](http://supervisord.org/configuration.html#programx-section) 设置以获取其他允许的键，除去上述的限制和添加。

### `[fcgi-program:x]`示例

```ini
[fcgi-program:fcgiprogramname]
command=/usr/bin/example.fcgi
socket=unix:///var/run/supervisor/%(program_name)s.sock
socket_owner=chrism
socket_mode=0700
process_name=%(program_name)s_%(process_num)02d
numprocs=5
directory=/tmp
umask=022
priority=999
autostart=true
autorestart=unexpected
startsecs=1
startretries=3
exitcodes=0
stopsignal=QUIT
stopasgroup=false
killasgroup=false
stopwaitsecs=10
user=chrism
redirect_stderr=true
stdout_logfile=/a/path
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stdout_events_enabled=false
stderr_logfile=/a/path
stderr_logfile_maxbytes=1MB
stderr_logfile_backups=10
stderr_events_enabled=false
environment=A="1",B="2"
serverurl=AUTO
```

## `[eventlistener:x]`设置

Supervisor允许在配置文件中定义专门的同类进程组（"事件监听器池"）。这些池包含了用于接收和响应supervisor事件系统的事件通知的进程。有关 [Event](http://supervisord.org/events.html#events) 的工作原理以及如何实现可声明为事件监听器的程序，请参阅事件文档。

请注意，除了 `stdout_capture_maxbytes` 以外，事件监听器部分尊重 `[program:x]` 部分可用的所有选项。事件监听器无法通过标准输出（`stdout`）发出进程通信事件，但可以通过标准错误输出（`stderr`）发出（参见捕获模式）。

### `[eventlistener:x]`值

`[eventlistener:x]` 部分有一些 `[program:x]` 部分没有的键。

`buffer_size`

事件监听器池的事件队列缓冲区大小。当监听器池的事件缓冲区溢出时（当事件监听器池无法跟上发送给所有事件时可能会发生），最旧的事件将被丢弃。

`events`

一个逗号分隔的事件类型名称列表，表示该监听器希望接收通知的“感兴趣”事件类型（请参阅 [Event Type](http://supervisord.org/events.html#event-types) 以获取有效的事件类型名称列表）。

`result_handler`

一个 [pkg_resources entry point string](http://peak.telecommunity.com/DevCenter/PkgResources)，可解析为Python可调用对象。默认值为 `supervisor.dispatchers:default_handler`。指定替代的结果处理程序是一件非常罕见的事情，因此如何创建并没有记录文档。

请参考 [[program:x]](http://supervisord.org/configuration.html#programx-section) 设置以获取其他允许的键，排除上述的限制和添加。

### `[eventlistener:x]`示例

```ini
[eventlistener:theeventlistenername]
command=/bin/eventlistener
process_name=%(program_name)s_%(process_num)02d
numprocs=5
events=PROCESS_STATE
buffer_size=10
directory=/tmp
umask=022
priority=-1
autostart=true
autorestart=unexpected
startsecs=1
startretries=3
exitcodes=0
stopsignal=QUIT
stopwaitsecs=10
stopasgroup=false
killasgroup=false
user=chrism
redirect_stderr=false
stdout_logfile=/a/path
stdout_logfile_maxbytes=1MB
stdout_logfile_backups=10
stdout_events_enabled=false
stderr_logfile=/a/path
stderr_logfile_maxbytes=1MB
stderr_logfile_backups=10
stderr_events_enabled=false
environment=A="1",B="2"
serverurl=AUTO
```

## `[rpcinterface:x]`设置

在配置文件中添加 `rpcinterface:x` 设置只对那些希望使用额外自定义行为扩展Supervisor的人有用。

在示例配置文件（参见 [创建配置文件](http://supervisord.org/installing.html#create-config)）中，有一个名为 `[rpcinterface:supervisor]` 的部分。默认情况如下所示。

```ini
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
```

为了使Supervisor的标准设置正常工作，`[rpcinterface:supervisor]` 部分必须保留在配置中。如果不希望Supervisor执行任何超出默认功能范围之外的操作，那么这就需要了解的关于这种类型部分的全部内容。

然而，如果希望添加RPC接口命名空间以自定义Supervisor，则可以添加额外的 `[rpcinterface:foo]` 部分，其中“foo”表示接口的命名空间（从Web根目录开始），并且由 `supervisor.rpcinterface_factory` 指定的值是一个工厂可调用对象，其函数签名应接受一个位置参数supervisord和足够多的关键字参数以进行配置。在 `[rpcinterface:x]` 部分中定义的任何额外键/值对都将作为关键字参数传递给工厂函数。

以下是一个示例工厂函数的例子，如在Python包 `my.package` 的 `__init__.py` 文件中创建。

```py
from my.package.rpcinterface import AnotherRPCInterface

def make_another_rpcinterface(supervisord, **config):
    retries = int(config.get('retries', 0))
    another_rpc_interface = AnotherRPCInterface(supervisord, retries)
    return another_rpc_interface
```

以下是配置文件中用于配置工厂函数的部分示例。

```ini
[rpcinterface:another]
supervisor.rpcinterface_factory = my.package:make_another_rpcinterface
retries = 1
```

### `[rpcinterface:x]`值

`supervisor.rpcinterface_factory`

`pkg_resources` 中的"入口点"（entry point）是指向RPC接口工厂函数的完整名称（使用点分隔）。

- 默认值：N/A。
- 必需项：否。
- 引入版本：3.0。

### `[rpcinterface:x]`示例

```ini
[rpcinterface:another]
supervisor.rpcinterface_factory = my.package:make_another_rpcinterface
retries = 1
```