---
title: 安装
order: 2
---

# 安装

## 有网安装

### 使用 `pip install` 安装：

```shell
pip install supervisor
```

根据系统 Python 的权限，可能需要以 root 用户身份使用 pip 成功安装 Supervisor。

你也可以通过 pip 在虚拟环境中安装 supervisor。

### 不使用 `pip install` 安装：

如果系统没有安装 `pip`，需要手动下载 Supervisor 发行版并进行安装。可以从 [PyPi](https://pypi.org/pypi/supervisor/) 下载当前和先前的 Supervisor 发行版。在解压软件存档之后，运行 `python setup.py install` 命令。这需要互联网访问。将下载和安装 Supervisor 所依赖的所有分发，并最终安装 Supervisor 本身。

:::info{title=提示}
根据系统 Python 的权限，可能需要以 root 用户身份成功执行 `python setup.py install` 命令。
:::

## 无网安装

### 使用 `pip install` 安装：

### 不使用 `pip install` 安装：

如果想要安装 Supervisor 的系统没有互联网访问，需要稍微改变安装方式。由于 `pip` 和 `python setup.py install` 都依赖于互联网访问来下载所需的软件包，因此在没有互联网访问的机器上，都无法正常工作，直到安装了依赖项为止。要在没有互联网连接的机器上进行安装，请在具有互联网连接的机器上获取以下依赖项：

- 从 [ https://pypi.org/pypi/setuptools/](https://pypi.org/pypi/setuptools/) 安装最新版本。

将这些文件复制到可移动介质上，并放在目标机器上。按照每个文件的说明在目标机器上进行安装。通常只需解压每个文件并在解压后的目录中运行 `python setup.py install`。最后，运行 Supervisor 的 `python setup.py install` 命令。

:::info{title=提示}
根据系统 Python 的权限，可能需要以 root 用户身份成功运行 `python setup.py install` 命令来安装每个软件包。
:::

## 安装分发包

一些 Linux 发行版通过系统软件包管理器提供了可安装的 Supervisor 版本。这些软件包由第三方制作，而不是 Supervisor 开发人员，并且通常包含对 Supervisor 进行的特定于发行版的更改。

使用发行版的软件包管理工具来检查 Supervisor 的可用性。例如，在 Ubuntu 上，可以运行 `apt-cache show supervisor`，在 CentOS 上，可以运行 `yum info supervisor`。

yum epel-release 安装 supervisor 示例：

```shell
yum install -y epel-release && yum install -y supervisor
```

Supervisor 的发行版软件包的一个特点是，通常会将其集成到发行版的服务管理基础设施中，例如允许 `supervisord` 在系统启动时自动启动。

:::info{title=提示}
发行版的 Supervisor 软件包在发布到 PyPI 的官方 Supervisor 软件包之后可能会滞后很长时间。例如，Ubuntu 12.04（于 2012 年 4 月发布）提供了基于 Supervisor 3.0a8（于 2010 年 1 月发布）的软件包。这种滞后通常是由发行版设定的软件发布策略引起的。
:::

:::info{title=提示}
用户报告称，针对 Ubuntu 16.04 的 Supervisor 发行版软件包与以前版本的行为不同。在 Ubuntu 10.04、12.04 和 14.04 上，安装该软件包将配置系统在启动时启动 supervisord。但在 Ubuntu 16.04 中，最初发布的软件包没有执行此操作。后来修复了该软件包。有关更多信息，请参阅 [Ubuntu Bug #1594740](https://bugs.launchpad.net/ubuntu/+source/supervisor/+bug/1594740)。
:::

## NPM安装

```shell
npm install -g supervisor
```

## Yarn安装

```shell
npm i -g yarn
yarn global add supervisor
```

## PNPM安装

```shell
npm i -g pnpm
pnpm install -g supervisor
```

## 创建配置文件

在 Supervisor 安装完成后，运行 `echo_supervisord_conf` 命令。将会在终端的标准输出中打印一个"示例" Supervisor 配置文件。

在终端中看到文件输出后，重新执行命令 `echo_supervisord_conf > /etc/supervisord.conf`。如果没有 root 访问权限，则无法执行此操作。

如果没有 root 访问权限，或者不想将 `supervisord.conf` 文件放在 `/etc/supervisord.conf` 中，可以将其放置在当前目录中（使用命令 `echo_supervisord_conf > supervisord.conf`），然后使用 `-c` 标志启动 supervisord 来指定配置文件的位置。

例如，使用命令 `supervisord -c supervisord.conf`。在这种情况下，实际上使用 `-c` 标志是多余的，因为 supervisord 会首先在当前目录中搜索 `supervisord.conf` 文件，然后再在其他位置搜索该文件，但也可以工作。有关 `-c` 标志的更多信息，请参阅 [运行 Supervisor](http://supervisord.org/running.html#running)。

一旦在文件系统中拥有配置文件，就可以开始按照自己的喜好进行修改。
