---
title: 介绍
order: 1
---

## 概述

Supervisor 是一个客户端/服务器系统，允许用户在类UNIX操作系统上控制多个进程。

**便利性**

需要为每个单独的进程实例编写 `rc.d` 脚本通常是不方便的。`rc.d` 脚本是一种最通用的进程初始化/自动启动/管理形式，但是编写和维护可能很麻烦。此外，`rc.d` 脚本无法自动重新启动崩溃的进程，而且许多程序在崩溃后无法正确地自我重启。Supervisord 将进程作为其子进程启动，并可以配置为在进程崩溃时自动重新启动。还可以自动配置为在自身调用时启动进程。

**准确性**

在 UNIX 系统上，通常很难获取进程的准确的上/下状态。`Pid` 文件经常会提供错误的信息。`Supervisord` 将进程作为子进程启动，因此始终知道其子进程的真实的上/下状态，并且可以方便地查询这些数据。

**委托**

监听“低”TCP端口的进程通常需要以 `root` 用户身份启动和重新启动（这是 UNIX 的一个缺陷）。通常情况下，允许“普通”用户停止或重新启动此类进程是完全可以接受的，但提供 `shell` 访问权限通常是不可行的，而提供 `root` 或 `sudo` 访问权限则往往是不可能的。同时，很难解释为什么会存在这个问题。如果以 `root` 用户身份启动 `supervisord`，则可以允许“普通”用户在不需要解释问题复杂性的情况下控制这些进程。`Supervisorctl` 允许以非常有限的方式访问计算机，基本上允许用户通过简单的 `Shell` 或 `Web` 用户界面发出 `stop` 、`start` 和 `restart` 命令，以查看进程状态并控制由 `supervisord` 控制的子进程。

**进程组**

通常需要以组的形式启动和停止进程，有时甚至需要按照“优先顺序”进行操作。解释如何执行此操作通常很困难。`Supervisord` 允许为进程分配优先级，并允许用户通过 `supervisorctl` 客户端发出诸如 `start all` 和 `restart all` 之类的命令，按照预先分配的优先级顺序启动。此外，进程可以分组为“process groups”，一组逻辑相关的进程可以作为一个单元停止和启动。

## 特点

**简单**

Supervisor 通过一个简单易学的 `INI` 格式配置文件进行配置。提供了许多针对进程的选项，如重新启动失败的进程和自动日志回滚。

**集中化**

Supervisor 提供一个统一的位置来启动、停止和监控进程。进程可以单独或以组的形式进行控制。可以配置 Supervisor 提供本地或远程的命令行和 Web 界面。

**高效**

Supervisor 通过 `fork/exec` 启动其子进程，而子进程不会进行守护化（daemonize）。操作系统会立即向 Supervisor 发送信号，通知其进程终止，与一些依赖于麻烦的 `PID` 文件和定期轮询来重新启动失败进程的解决方案不同。

**可扩展**

Supervisor 提供了一个简单的事件通知协议，任何编程语言编写的程序都可以使用该协议来监控，并提供了用于控制的 `XML-RPC` 接口。此外，Supervisor 还具有可供 Python 开发人员利用的扩展点。

**兼容性**

Supervisor 几乎在所有除 Windows 以外的平台上都可以运行。经过在 Linux、Mac OS X、Solaris 和 FreeBSD 上的测试和支持。完全使用 Python 编写，因此安装不需要 C 编译器。

**实践验证**

虽然 Supervisor 在当前仍在积极开发中，但并不是新的软件。Supervisor 已经存在多年，并且已经在许多服务器上使用。

## Supervisor 组件

**supervisord**

Supervisor 的服务器部分被称为 `supervisord`。负责在自身调用时启动子程序，响应来自客户端的命令，重新启动崩溃或退出的子进程，记录子进程的 `stdout` 和 `stderr`，并生成和处理与子进程生命周期中的特定点对应的 “events”。

服务器进程使用一个配置文件，通常位于 `/etc/supervisord.conf`。该配置文件是一个“Windows-INI”风格的配置文件。通过适当的文件系统权限保护此文件非常重要，因为可能包含未加密的用户名和密码信息。

**supervisorctl**

Supervisor 的命令行客户端部分被称为 `supervisorctl`。提供了一个类似 `shell` 的界面，用于访问 `supervisord` 提供的功能。通过 `supervisorctl`，用户可以连接到不同的 `supervisord` 进程（一次只能连接一个），获取由其控制的子进程的状态，停止和启动子进程，并获取 `supervisord` 中正在运行的进程列表。

命令行客户端通过 UNIX 域socket或因特网（TCP）socket与服务器进行通信。服务器可以要求客户端用户在执行命令之前提供身份验证凭据。客户端进程通常使用与服务器相同的配置文件，但任何带有 `[supervisorctl]` 部分的配置文件都可以使用。

**Web 服务**

如果使用 `Internet` socket启动 `supervisord`，则可以通过浏览器访问与 `supervisorctl` 相当的（简洁）Web 用户界面。在激活配置文件中的 `[inet_http_server]` 部分后，访问服务器 URL（例如 `http://localhost:9001/`）即可通过 Web 界面查看和控制进程状态。

**XML-RPC 接口**

相同的 HTTP 服务器既提供 Web 用户界面，也提供用于查询和控制 supervisor 及其运行的程序的 XML-RPC 接口。请参阅 [XML-RPC API 文档](http://supervisord.org/api.html#xml-rpc)。

## 平台要求

Supervisor 经过测试，已知可在 Linux（Ubuntu 18.04）、Mac OS X（10.4/10.5/10.6）和 Solaris（10 for Intel）以及 FreeBSD 6.1 上运行。也能够在大多数 UNIX 系统上正常工作。

Supervisor 在任何版本的 Windows 下都无法运行。

Supervisor 旨在在 Python 3 的 3.4 版本或更高版本上运行，以及在 Python 2 的 2.7 版本上运行。