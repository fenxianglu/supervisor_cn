---
title: 子进程
order: 6
---

supervisord的主要目的是根据配置文件中的数据创建和管理进程。通过创建子进程来实现这一目的。由supervisor生成的每个子进程在其整个生命周期中都由supervisord管理（supervisord是创建的每个进程的父进程）。当子进程结束时，supervisor通过 `SIGCHLD` 信号接收到其终止的通知，并执行相应的操作。

## 无守护子进程

在supervisor下运行的程序不应该自行进行守护化。相反，应该在前台运行，并且不应该从启动终端分离。

判断一个程序是否在前台运行的最简单方法是从Shell提示符中运行调用该程序的命令。如果将控制权返回，但仍然继续运行，那么其正在自行进行守护化，几乎肯定不适合在supervisor下运行。希望运行的命令基本上需要按 `Ctrl-C` 来重新获得对终端的控制权。如果在运行该命令后，无需按 `Ctrl-C` 即可返回一个Shell提示符，则在supervisor下没有用处。所有程序都有在前台运行的选项，但没有“标准方式”来实现；需要阅读每个程序的文档。

以下是已知在Supervisor下以“前台”模式启动常见程序的配置文件示例。

### 进程配置示例

以下是一些“real world”配置的示例：

#### Apache 2.2.6

```ini
[program:apache2]
command=/path/to/httpd -c "ErrorLog /dev/stdout" -DFOREGROUND
redirect_stderr=true
```

#### Two Zope 2.X instances and one ZEO server

```ini
[program:zeo]
command=/path/to/runzeo
priority=1

[program:zope1]
command=/path/to/instance/home/bin/runzope
priority=2
redirect_stderr=true

[program:zope2]
command=/path/to/another/instance/home/bin/runzope
priority=2
redirect_stderr=true
```

#### Postgres 8.X¶

```ini
[program:postgres]
command=/path/to/postmaster
; we use the "fast" shutdown signal SIGINT
stopsignal=INT
redirect_stderr=true
```

#### OpenLDAP slapd

```ini
[program:slapd]
command=/path/to/slapd -f /path/to/slapd.conf -h ldap://0.0.0.0:8888
redirect_stderr=true
```

### 其他示例

可以在 [http://thedjbway.b0llix.net/services.html](http://thedjbway.b0llix.net/services.html) 找到其他用于在supervisord下启动服务的shell脚本示例。这些示例实际上是针对daemontools的，但是在supervisord中的原理是相同的。

可以从 [http://smarden.org/runit/runscripts.html](http://smarden.org/runit/runscripts.html) 获取另一个用于在前台启动各种程序的脚本集合。

## pidproxy程序

一些进程（如mysqld）会忽略由supervisord生成的实际进程所发送的信号。相反，这类程序会创建一个“特殊”的线程/进程来处理信号。这是有问题的，因为supervisord只能终止创建的进程。如果由supervisord创建的进程创建了自己的子进程，supervisord无法终止。

幸运的是，这类程序通常会写入一个“pidfile”文件，其中包含了“特殊”进程的PID，并且可以通过读取和使用该文件来终止进程。作为对这种情况的解决方案，可以使用特殊的pidproxy程序来处理这类进程的启动。pidproxy程序是一个小型桥接程序，启动一个进程，并在收到信号时将信号发送给pidfile中提供的PID。下面是一个启用pidproxy的程序的示例配置项：

```ini
[program:mysql]
command=/path/to/pidproxy /path/to/pidfile /path/to/mysqld_safe
```

pidproxy程序在安装supervisor时会放置在配置的 `$BINDIR` 目录中（是一个“console script”）。

## 子进程环境

子进程将继承用于启动supervisord程序的shell环境。supervisord本身将在子进程的环境中设置几个环境变量，包括 `SUPERVISOR_ENABLED`（表示进程受supervisor控制的标志）、`SUPERVISOR_PROCESS_NAME`（指定该进程的配置文件中的进程名称）和 `SUPERVISOR_GROUP_NAME`（指定子进程的配置文件中的进程组名称）。

这些环境变量可以在 `[supervisord]` 部分的配置选项中使用名为environment（适用于所有子进程）或每个[program:x]部分的environment配置选项中进行覆盖（仅适用于在 `[program:x]` 部分中指定的子进程）。这些“environment”设置是可累加的。换句话说，每个子进程的环境将包括以下内容：

在启动supervisord时所使用的shell中设置的环境变量...

...会被添加到/覆盖为...

...在全局的“environment”设置中所设置的环境变量添加到/覆盖为...

...在配置选项中...

...被添加到/被覆盖为...

...supervisor特定的环境变量...

(`SUPERVISOR_ENABLED`, `SUPERVISOR_PROCESS_NAME`, `SUPERVISOR_GROUP_NAME`) 等...

...被添加到/覆盖为...

...在每个进程的环境变量设置中添加到/覆盖为...

“environment”配置选项中...

当supervisord运行子进程时，不会执行任何shell，因此环境变量（如USER、PATH、HOME、SHELL、LOGNAME等）不会从默认值更改或重新分配。这一点特别重要的是在配置中将supervisord以root身份运行，并在 `user=` 段落中指定用户时需要注意。与cron不同，当supervisord执行setuid到 `user=` 程序配置选项中定义的用户时，不会尝试去理解和覆盖“基本”环境变量，例如USER、PATH、HOME和LOGNAME。如果需要为特定程序设置环境变量，而这些变量可能会在特定用户的shell调用中设置，必须在environment=程序配置选项中显式地设置。以下是设置这些环境变量的示例：

```ini
[program:apache2]
command=/home/chrism/bin/httpd -c "ErrorLog /dev/stdout" -DFOREGROUND
user=chrism
environment=HOME="/home/chrism",USER="chrism"
```

## 进程状态

由supervisord控制的进程在任何给定时间将处于以下状态之一。可能会在客户端的各种用户界面元素中看到这些状态名称。

`STOPPED` (0)

该进程已因停止请求而停止，或者从未启动过。

`STARTING` (10)

由于启动请求，该进程正在启动。

`RUNNING` (20)

该进程正在运行。

`BACKOFF` (30)

该进程进入了STARTING状态，但在之后很快退出（在startsecs定义的时间内），无法转移到RUNNING状态。

`STOPPING` (40)

由于停止请求，该进程正在停止。

`EXITED` (100)

该进程从RUNNING状态退出（无论是预期的还是意外的）。

`FATAL` (200)

该进程无法成功启动。

`UNKNOWN` (1000)

该进程处于未知状态（supervisord编程错误）。

每个在supervisor下运行的进程根据以下有向图按照这些状态进行变化。

![](./images/subprocess-transitions.png)

*子进程状态转换图*

如果进程已被管理员停止或从未启动，则处于 `STOPPED` 状态。

当一个自动重启的进程处于 `BACKOFF` 状态时，supervisord会自动重新启动。将在 `STARTING` 和 `BACKOFF` 状态之间切换，直到明确无法启动，因为启动重试次数已超过最大值，此时将转移到 `FATAL` 状态。

:::info{title=提示}
重试的时间将根据后续尝试次数增加，每次增加一秒。

因此，如果设置了 `startretries=3`，supervisord将在每次重新启动尝试之间等待一秒、两秒和三秒，总计为5秒。
:::

当进程处于 `EXITED` 状态时，将自动重新启动：

- 如果其 `autorestart` 参数设置为 `false`，则永远不会自动重新启动。
- 无条件地重新启动，如果其 `autorestart` 参数设置为 `true`。
- 根据其 `autorestart` 参数的设置，如果设置为 `unexpected`，则条件地重新启动。如果进程以与在 `exitcodes` 配置参数中定义的退出码不匹配的退出码退出，则将重新启动该进程。

一个进程会由于被配置为有条件或无条件地自动重新启动而自动从 `EXITED` 状态转换到 `RUNNING` 状态。在 `RUNNING` 和 `EXITED` 之间的转换次数没有任何限制：可以创建一个无限重启已退出进程的配置。这是一种特性，而不是错误。

如果一个自动重启的进程进入 `FATAL` 状态，将不会自动重新启动（必须手动从该状态重新启动）。

进程通过管理停止请求进入 `STOPPING` 状态，然后最终进入 `STOPPED` 状态。

无法成功停止的进程将永远停留在 `STOPPING` 状态。在正常操作过程中，不应该出现这种情况，因为这意味着进程没有响应supervisor发送最后一个 `SIGKILL` 信号，而在UNIX系统下这是“不可能”的情况。

总是需要用户主动触发的状态转换包括：

`FATAL` -> `STARTING`

`RUNNING` -> `STOPPING`

通常情况下需要用户主动触发的状态转换包括以下内容，但也有一些例外情况：

`STOPPED` -> `STARTING`（除非在supervisord启动时，如果进程被配置为自动启动）

`EXITED` -> `STARTING`（除非进程被配置为自动重启）