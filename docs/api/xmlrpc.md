---
title: XML-RPC
order: 9
---

要使用 `XML-RPC` 接口，请首先确保已正确配置接口工厂并设置默认工厂。请参阅[《配置 XML-RPC 接口工厂》](http://supervisord.org/xmlrpc.html#rpcinterface-factories)获取更多信息。

然后可以使用任何 `XML-RPC` 客户端库连接到 `Supervisor` 的 `HTTP` 端口，并对其运行命令。

使用 Python 2 的 `xmlrpclib` 客户端库执行此操作的示例如下所示。

```py
import xmlrpclib
server = xmlrpclib.Server('http://localhost:9001/RPC2')
```

使用 Python 3 的 `xmlrpc.client` 客户端库执行此操作的示例如下所示。

```py
from xmlrpc.client import ServerProxy
server = ServerProxy('http://localhost:9001/RPC2')
```

可以使用 `supervisor` 命名空间调用 `supervisord` 及其子进程的方法。下面提供了一个示例：

```py
server.supervisor.getState()
```

可以使用 `XML-RPC` 的 `system.listMethods` API 来获取 supervisord XML-RPC 接口支持的方法列表：

```py
server.system.listMethods()
```

可以使用 `system.methodHelp` API 对方法调用以获取其帮助信息：

```py
server.system.methodHelp('supervisor.shutdown')
```

supervisord 的 XML-RPC 接口还支持 [XML-RPC multicall API](http://web.archive.org/web/20060824100531/http://www.xmlrpc.com/discuss/msgReader$1208)。

可以根据需要添加新的顶级 RPC 接口来扩展 supervisord 的功能，以增加新的 XML-RPC API 方法。请参阅[《配置 XML-RPC 接口工厂》](http://supervisord.org/xmlrpc.html#rpcinterface-factories)。

:::info{title=提示}
任何 XML-RPC 方法调用都可能导致错误响应。这包括由客户端引起的错误，如参数错误，以及 supervisord 无法满足请求的任何错误。许多 XML-RPC 客户端程序在遇到错误响应时会引发异常。
:::

## 状态和控制

`classsupervisor.rpcinterface.SupervisorNamespaceRPCInterface(supervisord)`

`getAPIVersion()`

返回 supervisord 使用的 RPC API 版本。

@return string version id

此 API 的版本与 Supervisor 本身的版本是独立进行版本控制的。`getAPIVersion` 返回的 API 版本仅在 API 发生变化时才会更改。其目的是帮助客户端确定与 Supervisor API 的哪个版本进行通信。

在编写与此 API 进行通信的软件时，强烈建议先测试兼容性，然后再进行方法调用。

:::info{title=提示}
`getAPIVersion` 方法替代了之前版本的 Supervisor 中的 `getVersion` 方法（在 3.0a1 之前的版本中），但是 `getVersion()` 已被弃用，并且在将来的版本中将不再支持。
:::

`getSupervisorVersion()`

返回 supervisord 使用的 supervisor 包版本。

@return string version id

`getIdentification()`

返回 supervisord 的标识字符串。

@return string identifier identifying string

该方法允许客户端在可能存在多个运行的 Supervisor 实例的环境中确定与哪个 Supervisor 实例进行通信。

这个标识是在 Supervisor 的配置文件中进行设置的字符串。该方法简单地将该值返回给客户端。

`getState()`

以结构体形式返回 supervisord 的当前状态。

@return struct A struct with keys int statecode, string statename

这是由 Supervisor 维护的内部值，用于确定 Supervisor 认为自己的当前操作状态是什么。

某些方法调用可以改变 Supervisor 的当前状态。例如，当 Supervisor 处于 RUNNING 状态时调用 `supervisor.shutdown()` 方法会将 Supervisor 放置在 SHUTDOWN 状态，而此时正在关闭过程中。

`supervisor.getState()` 方法为客户端提供了一种检查 Supervisor 状态的方式，既用于信息目的，也用于确保打算调用的方法将被允许。

返回值是一个结构体：

```js
{'statecode': 1,
 'statename': 'RUNNING'}
```

可能的返回值有：

| 状态 | 状态名称 | 描述 |
|:- |:- |:- |
| 2 | FATAL | Supervisor 遇到了严重的错误。 |
| 1 | RUNNING | Supervisor 正常工作。 |
| 0 | RESTARTING | Supervisor 正在重新启动的过程中。 |
| -1 | SHUTDOWN | Supervisor 正在关闭的过程中。 |

`FATAL` 状态报告了不可恢复的错误，比如 `Supervisor` 内部错误或系统失控情况。一旦设置为 `FATAL`，`Supervisor` 就无法在不重启的情况下返回到其他任何状态。

在 `FATAL` 状态下，除了 `supervisor.shutdown()` 和 `supervisor.restart()` 方法之外，所有未被调用的后续方法都将自动失败，并引发故障 `FATAL_STATE`。

在 `SHUTDOWN` 或 `RESTARTING` 状态下，所有方法调用都将被忽略，并且返回值是未定义的。

`getPID()`

返回 `supervisord` 的进程 `ID`（PID）。

@return int PID

`readLog(offset, length)`

从主日志的偏移位置开始，读取指定长度的字节数。

`@param int offset` 起始读取偏移量。
`@param int length` 从日志中要读取的字节数。
`@return string result` 日志的字节内容。

该方法可以返回整个日志、日志末尾的一定数量字符，或根据偏移量和长度参数指定的日志片段。

| Offset | Length | Behavior of `readProcessLog` |
|:- |:- |:- |
| Negative | Not Zero | 参数错误。会引发 `BAD_ARGUMENTS` 故障。 |
| Negative | Zero | 返回日志的末尾，或者从日志末尾开始偏移指定数量的字符。例如，如果 `offset = -4` 且 `length = 0`，则将从日志末尾返回最后四个字符。 |
| Zero 或 Positive | Negative | 参数错误。会引发 `BAD_ARGUMENTS` 故障。 |
| Zero 或 Positive | Zero | 将从指定的偏移位置开始返回所有字符。 |
| Zero 或 Positive | Positive | 将从指定偏移位置返回一定数量的字符长度。 |

如果日志为空且要求返回整个日志，则返回空字符串。

如果偏移量或长度超出范围，将返回 `BAD_ARGUMENTS` 故障。

如果无法读取日志，该方法将引发 `NO_FILE` 错误（如果文件不存在）或 `FAILED` 错误（如果遇到其他任何问题）。

:::info{title=提示}
`readLog()` 方法替代了在 Supervisor 2.1 之前版本中找到的 `readMainLog()` 方法。为了兼容性而提供了别名，但是 `readMainLog()` 方法已经被弃用，并且在将来的版本中将不再支持。
:::

`clearLog()`

清空主日志。

@return boolean result always returns True unless error

如果无法清空日志，因为日志文件不存在，将引发 `NO_FILE` 故障。如果由于其他原因无法清空日志，则会引发 `FAILED` 故障。

`shutdown()`

关闭 Supervisor 进程。

@return boolean result always returns True unless error

该方法关闭 Supervisor 守护进程。如果有任何正在运行的进程，将被自动终止而无需提前警告。

与大多数其他方法不同，即使 Supervisor 处于 `FATAL` 状态，该方法仍然可用。

`restart()`

重新启动 Supervisor 进程。

@return boolean result always return True unless error

该方法对 Supervisor 守护进程进行软重启。如果有任何正在运行的进程，将被自动终止而无需提前警告。请注意，实际的 UNIX 进程不能重新启动；只有 Supervisor 的主程序循环会重新启动。这会导致 Supervisor 的内部状态重置。

与大多数其他方法不同，即使 Supervisor 处于 `FATAL` 状态，该方法仍然可用。

## 进程控制

`classsupervisor.rpcinterface.SupervisorNamespaceRPCInterface(supervisord)`

`getProcessInfo(name)`

获取名为 name 的进程信息。

`@param string name` 进程的名称（或 'group:name'）。
`@return struct result` 包含有关进程数据的结构。

返回值是一个结构体：

```js
{'name':           'process name',
 'group':          'group name',
 'description':    'pid 18806, uptime 0:03:12'
 'start':          1200361776,
 'stop':           0,
 'now':            1200361812,
 'state':          20,
 'statename':      'RUNNING',
 'spawnerr':       '',
 'exitstatus':     0,
 'logfile':        '/path/to/stdout-log', # deprecated, b/c only
 'stdout_logfile': '/path/to/stdout-log',
 'stderr_logfile': '/path/to/stderr-log',
 'pid':            1
}
```

`name`

进程的名称。

`group`

进程组名称。

`description`

如果进程状态为运行中，描述的值是进程 ID 和运行时间。例如：“pid 18806, uptime 0:03:12”。
如果进程状态为停止，描述的值是停止时间。例如：“Jun 5 03:16 PM”。

`start`

进程启动的 UNIX 时间戳。

`stop`

进程上次结束的 UNIX 时间戳，如果进程从未停止过，则为0。

`now`

当前时间的 UNIX 时间戳，可用于计算进程的运行时间。

`state`

状态代码，参见[进程状态](http://supervisord.org/subprocess.html#process-states)。

`statename`

状态的字符串描述，参见[进程状态](http://supervisord.org/subprocess.html#process-states)。

`logfile`

已弃用的 `stdout_logfile` 的别名。这仅提供与为 Supervisor 2.x 编写的客户端的兼容性，并可能在将来被移除。请改用 `stdout_logfile`。

`stdout_logfile`

STDOUT 日志文件的绝对路径和文件名。

`stderr_logfile`

STDOUT 日志文件的绝对路径和文件名。

`spawnerr`

在生成过程中发生的错误描述，如果没有错误，则为空字符串。

`exitstatus`

进程的退出状态（错误级别），如果进程仍在运行，则为0。

`pid`

进程的 UNIX 进程 ID（PID），如果进程未运行，则为0。

`getAllProcessInfo()`

获取所有进程的信息。

`@return array result` 一个包含进程状态结果的数组。

每个元素都包含一个结构体，该结构体与 `getProcessInfo` 返回的结构体具有完全相同的元素。如果进程表为空，则返回一个空数组。

`getAllConfigInfo()`

获取所有可用进程配置的信息。每个结构表示一个单独的进程（即，组会被展开成单个进程）。

`@return array result` 一个包含进程配置信息结构的数组。

`startProcess(name, wait=True)`

启动一个进程。

`@param string name` 进程名称（或组：名称，或组:*）。
`@param boolean wait` 等待进程完全启动。
`@return boolean result` 始终为 true，除非出现错误。

`startAllProcesses(wait=True)`

启动配置文件中列出的所有进程。

`@param boolean wait` 等待每个进程完全启动。
`@return array result` 一个包含进程状态信息结构的数组。

`startProcessGroup(name, wait=True)`

启动名为 'name' 的进程组中的所有进程。

`@param string name` 组名
`@param boolean wait` 等待每个进程完全启动
`@return array result` 一个包含进程状态信息结构的数组

`stopProcess(name, wait=True)`

停止名为 name 的进程。

`@param string name` 要停止的进程的名称（或 'group:name'）。
`@param boolean wait` 等待进程完全停止。
`@return boolean result` 始终返回 True，除非出现错误。

`stopProcessGroup(name, wait=True)`

停止名为 'name' 的进程组中的所有进程。

`@param string name` 进程组名称
`@param boolean wait` 等待每个进程完全停止
`@return array result` 一个包含进程状态信息结构的数组

`stopAllProcesses(wait=True)`

停止进程列表中的所有进程。

`@param boolean wait` 等待每个进程完全停止
`@return array result` 一个包含进程状态信息结构的数组

`signalProcess(name, signal)`

向名为 name 的进程发送任意的 UNIX 信号。

`@param string name` 要发送信号的进程名称（或 'group:name'）。
`@param string signal` 要发送的信号，可以是名称（如 'HUP'）或数字（如 '1'）。
`@return boolean` 返回值为布尔型。

`signalProcessGroup(name, signal)`

向名为 'name' 的进程组中的所有进程发送信号。

`@param string name` 组名
`@param string signal` 要发送的信号，可以是信号名称（如 'HUP'）或数字（如 '1'）
`@return array` 返回值为数组类型。

`signalAllProcesses(signal)`

向进程列表中的所有进程发送信号。

`@param string signal` 要发送的信号，可以是信号名称（如 'HUP'）或数字（如 '1'）
`@return array result` 一个包含进程状态信息结构的数组

`sendProcessStdin(name, chars)`

向进程名称的标准输入(stdin)发送字符串。如果发送的数据为非7位数据（如Unicode），在发送到进程的标准输入之前会将其编码为UTF-8。如果 chars 不是字符串或不是Unicode，则引发 INCORRECT_PARAMETERS 错误。如果进程未运行，则引发 NOT_RUNNING 错误。如果进程的标准输入无法接受输入（例如，被子进程关闭了），则引发 NO_FILE 错误。

`@param string name` 要发送的进程名称（或 'group:name'）。
`@param string chars` 要发送给进程的字符数据。
`@return boolean result` 始终返回 True，除非出现错误。

`sendRemoteCommEvent(type, data)`

发送一个事件，该事件将由订阅 RemoteCommunicationEvent 的事件监听子进程接收到。

`@param string type` 事件头中的 "type" 键的字符串
`@param string data` 事件主体的数据
`@return boolean` 始终返回 True，除非发生错误。

`reloadConfig()`

重新加载配置。

结果包含三个数组，其中包含进程组的名称：

- added：包含已添加的进程组的名称。
- changed：包含内容发生更改的进程组的名称。
- removed：包含不再存在于配置中的进程组的名称。

@return array result [[added, changed, removed]]

`addProcessGroup(name)`

从配置文件更新正在运行的进程的配置。

`@param string name` 要添加的进程组的名称
`@return boolean result` 如果成功则返回 true

`removeProcessGroup(name)`

从活动配置中移除已停止的进程。

`@param string name` 要移除的进程组的名称
`@return boolean result` 指示移除是否成功

## 进程日志

`classsupervisor.rpcinterface.SupervisorNamespaceRPCInterface(supervisord)`

`readProcessStdoutLog(name, offset, length)`

从名称为 name 的进程的标准输出日志中，从偏移量 offset 开始读取 length 字节。

`@param string name` 进程的名称（或 'group:name'）。
`@param int offset` 要开始读取的偏移量。
`@param int length` 要从日志中读取的字节数。
`@return string result` 日志的字节数据。

`readProcessStderrLog(name, offset, length)`

从名称为 name 的进程的标准错误日志中，从偏移量 offset 开始读取 length 字节。

`@param string name` 进程的名称（或 'group:name'）。
`@param int offset` 从其开始读取的偏移量。
`@param int length` 要从日志中读取的字节数。
`@return string result` 日志的字节数据。

`tailProcessStdoutLog(name, offset, length)`

`tailProcessStdoutLog()` 提供了比 `readProcessStdoutLog()` 更高效的方式来尾随（stdout）日志。使用 `readProcessStdoutLog()` 读取块和 `tailProcessStdoutLog()` 进行尾随操作。

从名称为 (name) 的日志中请求 (length) 字节，起始位置为 (offset)。如果日志的总大小大于 (offset + length)，则设置溢出标志，并自动增加 (offset) 以将缓冲区定位在日志末尾。如果可用字节数少于 (length)，则返回最大可用字节数。返回的 (offset) 始终是日志中的最后一个偏移量 +1。

`@param string name` 进程的名称（或 'group:name'）。
`@param int offset` 要开始读取的偏移量。
`@param int length` 要返回的最大字节数。
`@return array result` [string bytes, int offset, bool overflow] 数组。其中，bytes 是读取到的字节数据，offset 是日志中的最后一个偏移量 +1，overflow 表示是否发生了溢出。

`tailProcessStderrLog(name, offset, length)`

`tailProcessStderrLog()` 提供了比 `readProcessStderrLog()` 更高效的方式来尾随（stderr）日志。使用 `readProcessStderrLog()` 读取块和 `tailProcessStderrLog()` 进行尾随操作。

从名称为 (name) 的日志中请求 (length) 字节，起始位置为 (offset)。如果日志的总大小大于 (offset + length)，则设置溢出标志，并自动增加 (offset) 以将缓冲区定位在日志末尾。如果可用字节数少于 (length)，则返回最大可用字节数。返回的 (offset) 始终是日志中的最后一个偏移量 +1。

`@param string name` 进程的名称（或 'group:name'）。
`@param int offset` 要开始读取的偏移量。
`@param int length` 要返回的最大字节数。
`@return array result` [string bytes, int offset, bool overflow] 数组。其中，bytes 是读取到的字节数据，offset 是日志中的最后一个偏移量 +1，overflow 表示是否发生了溢出。

`clearProcessLogs(name)`

清除指定进程的标准输出（stdout）和标准错误（stderr）日志，并重新打开。

`clearAllProcessLogs()`

清除所有进程日志文件。

`@return array result` 一个包含进程状态信息结构的数组。

## 系统方法

`classsupervisor.xmlrpc.SystemNamespaceRPCInterface(namespaces)`

`listMethods()`

返回一个列出可用方法名称的数组。

`@return array result` 可用方法名称的数组（字符串）。

`methodHelp(name)`

返回显示方法文档的字符串。

`@param string name` 方法的名称。
`@return string result` 方法名的文档。

`methodSignature(name)`

返回一个数组，描述方法的签名形式为 [rtype, ptype, ptype...]，其中 rtype 是方法的返回数据类型，ptypes 是方法按参数顺序接受的参数数据类型。

`@param string name` 方法的名称。
`@return array result` 结果。

`multicall(calls)`

处理一组调用，并返回结果数组。调用应该是以下形式的结构体：`{ 'methodName': string, 'params': array }`。每个结果要么是包含结果值的单项数组，要么是以下形式的结构体：`{'faultCode': int, 'faultString': string}`。当需要进行大量小规模的调用而避免大量的往返时非常有用。

`@param array calls` 调用请求的数组。
`@return array result` 结果的数组。