---
title: 升级
order: 10
---

当将 Supervisor 2.X 升级到 Supervisor 3.X 版本时，以下内容是正确的：

1. 在 `[program:x]` 部分中，键 `logfile`、`logfile_backups`、`logfile_maxbytes`、`log_stderr` 和 `log_stdout` 不再有效。Supervisor 2 将 `stderr` 和 `stdout` 记录到单个日志文件中，而 Supervisor 3 将 `stderr` 和 `stdout` 记录到不同的日志文件中。你需要至少将 `logfile` 重命名为 `stdout_logfile`，`logfile_backups` 重命名为 `stdout_logfile_backups`，`logfile_maxbytes` 重命名为 `stdout_logfile_maxbytes`，以保留你的配置。如果你创建了具有 `log_stderr` 为 `true` 的 `program` 部分，为了保持将 `stderr` 输出发送到 `stdout` 日志的行为，请在 `program` 部分中使用 `redirect_stderr` 布尔值来实现。

2. Supervisor 配置文件必须逐字包含以下部分，以确保 XML-RPC 接口（因此也包括 Web 界面和 supervisorctl）能够正常工作：

```ini
[rpcinterface:supervisor]
supervisor.rpcinterface_factory = supervisor.rpcinterface:make_main_rpcinterface
```

3. `[program:x]` 部分中的 `autorestart` 参数的语义已经发生变化。该参数过去只接受 `true` 或 `false`。另外还接受一个额外的值，`unexpected`，表示进程只有在其退出代码与进程配置中的 `exitcode` 参数所代表的任何退出代码不匹配（即进程崩溃）时，才从 `EXITED` 状态重新启动。此外，`autorestart` 的默认值现在是 `unexpected`（以前是 `true`，意味着无条件重新启动）。

4. 现在，我们允许 `supervisord` 同时在 UNIX 域socket和 inet socket上进行监听，而不是使其互斥地监听其中一个。因此，在 [supervisord] 部分的配置中，选项 `http_port`、`http_username`、`http_password`、`sockchmod` 和 `sockchown` 不再存在，被两个新的部分取代：[unix_http_server] 和 [inet_http_server]。你需要将其中之一（根据你想要在 `UNIX` 域socket上监听还是在 TCP socket上监听）或两者都插入到 `supervisord.conf` 文件中。这些部分有自己的选项（如果适用）用于 `port`、`username`、`password`、`chmod` 和 `chown`。

5. 已删除与 `http_port`、`http_username`、`http_password`、`sockchmod` 和 `sockchown` 相关的所有 `supervisord` 命令行选项（请参见上述原因）。

6. 在 [supervisord] 部分中过去被称为 `sockchown` 的选项（现在在 `[unix_http_server]` 部分中被称为 `chown`）曾接受一个点分隔的值（`user.group`）。现在，分隔符必须是冒号，例如 `user:group`。Unix 系统允许用户名中包含点，因此这个更改是修复了一个错误。