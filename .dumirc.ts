import { defineConfig } from 'dumi';

export default defineConfig({
  // git page server
  // base: '/supervisor_cn/',
  // publicPath: '/supervisor_cn/',
  favicons: [
    '/logo.svg'
  ],
  sitemap: {
    hostname: 'https://supervisor.fenxianglu.cn/'
  },
  headScripts: [
    {src: "https://www.fenxianglu.cn/build/js/inject.js", defer: 'defer'},
    {src: "https://pagead2.googlesyndication.com/pagead/js/adsbygoogle.js", "data-ad-client": "ca-pub-3955044083622262"}
  ],
  metas: [
    {
      name: "description",
      content: "supervisor中文网，supervisor中文文档，supervisord中文网，supervisord中文文档，supervisor进程管理工具，supervisord进程管理工具"
    },
    {
      name: "baidu-site-verification",
      content: "codeva-pPlEfTj7P2"
    },
    {
      name: "msvalidate.01",
      content: "D4BCABF292DC2FB7E49F914FF0CE6BAF"
    }
  ],
  analytics: {
    baidu: '25ec2e8df2d9682422b146eac25938b5'
  },
  themeConfig: {
    name: 'supervisor中文网',
    logo: '/logo.svg',
    nav: [
      { title: '介绍', link: '/api/introduction' },
      { title: '安装', link: '/api/installing' },
      { title: '运行', link: '/api/running' },
      { title: '配置', link: '/api/configuration' },
      { 
        title: '其他', 
        children: [
          {title: '事件', link: '/api/events'},
          {title: '子进程', link: '/api/subprocess'},
          {title: 'XML-RPC', link: '/api/xmlrpc'},
          { title: '插件', link: '/api/plugins' },
          {title: '日志', link: '/api/logging'},
          {title: '升级', link: '/api/upgrading'},
        ]
      },
      { title: '英文文档', link: 'http://supervisord.org/' },
      {
        title: '文档链接',
        children: [
          { title: 'PM2中文网', link: 'https://pm2.fenxianglu.cn/' },
          { title: 'Day.js中文网', link: 'https://dayjs.fenxianglu.cn/' },
          { title: 'Turf.js中文网', link: 'https://turfjs.fenxianglu.cn/' },
          { title: 'ArcGis中文网', link: 'https://arcgis.fenxianglu.cn/' },
          { title: 'element-plus中文网', link: 'https://elementplus.fenxianglu.cn/' },
          { title: 'gofiber中文网', link: 'https://gofiber.fenxianglu.cn/' },
          { title: 'React菜鸟文档', link: 'https://react.fenxianglu.cn/' },
          { title: 'Mock.js文档', link: 'https://mockjs.fenxianglu.cn/' },
        ]
      },
      {
        title: '友情链接',
        children: [
          { title: '技术开发分享录', link: 'https://www.fenxianglu.cn/' },
          { title: '免费字体', link: 'https://ziti.fenxianglu.cn/' },
          { title: '软考刷题', link: 'https://ruankao.fenxianglu.cn/' },
          { title: '资源下载', link: 'https://ziyuan.fenxianglu.cn/' },
        ]
      }
    ],
    footer: `Copyright © 2024 <a href='http://beian.miit.gov.cn/' target='_blank'>皖ICP备19023624号</a>`,
  },
});